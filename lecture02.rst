Lecture 2
=========

What Is Artificial Intelligence, Machine Learning, and Data Science Anyway?
---------------------------------------------------------------------------

You will probably find as many definitions of *artificial intelligence*, *machine learning*, and *data science* as you find people giving an opinion on these matters.

In this course we are not afraid of being controversial, so here are our bold definitions:

.. glossary::

    Artificial Intelligence (AI)
        Sounds very sexy and sci-fi-ish, the term we use to get funding, and what we all hope the future will bring.

    Machine Learning (ML)
        While not being too concerned with real-world problems, and being a classic scientific discipline, it's the part of AI that actually works

    Data Science:
        Applying machine learning to real-world problems, and glorified statistics.


In this course we will concern ourselves with practical and applicable use of artificial intelligence, thus the title *Applied Artificial Intelligence*.

Paradigms of Data Science
-------------------------

We have two major paradigms in Data Science:

.. glossary::

    Hypothesis-Driven Data Science
        We start with a hypothesis or a problem, and we then need to collect or find data for the research.

    Problem-Driven Data Science
        See Hypothesis-Drive Data Science

    Data-Driven Data Science
        We have a data set and want to find new insights from that data.

Data Science is a Process
-------------------------

There are several steps in the Data Science process:

.. figure:: _static/ds-deconstructed.jpeg
    :width: 600px
    :align: center
    :height: 644px
    :alt: The Data Science process
    :figclass: align-center

    The Data Science process


AJ Goldstein does a good job of describing the Data Science process:

`Goldstein, AJ. "Deconstructing Data Science: Breaking The Complex Craft Into It’s Simplest Parts" <https://medium.com/the-mission/deconstructing-data-science-breaking-the-complex-craft-into-its-simplest-parts-15b15420df21>`_

Definitions of Data Science
---------------------------

   *Data science is gaining more and more and widespread attention, but no consensus viewpoint on what data science is has emerged. As a new science, its objects of study and scientific issues should not be covered by established sciences. In the present paper, data science is defined as the science of exploring datanature. We believe this is the most logical and accurate definition of data science (...)*

   – Zhu, Yangyong, and Yun Xiong. "Defining data science." arXiv preprint arXiv:1501.05039 (2015).

   *Data Science is an application of scientific methods and principles to data processing.*
   - Henrik Strøm

Ethics
------

No, this is not going to be a social science class, however ...

    *– ethics is important – both in how you conduct your research, and in how you report and communicate it. Data Science is not tabloid journalism or politics.*

With great power comes great responsibility: be honest and follow the evidence!

Framing the problem
-------------------

Framing the problem is the most important part of a Data Science project.
You have to make sure that:

* You are asking a relevant question
* That your question is put into a relevant context
* It is actually possible to answer the question
* You can answer the question with the resources available to you

`Tayo, Benjamin Obi. "Problem Framing: The Most Difficult Stage of a Machine Learning Project Workflow" <https://medium.com/towards-artificial-intelligence/problem-framing-the-most-difficult-stage-of-machine-learning-1a7f208ea414>`_

.. warning:: If you fail at framing the problem, chances are that your project will fail!

You might think that framing the problem sets the direction of the project.
This is not true - it sets the *goal* or destination of the project.
If you fail at this, you don't know where you will end up, or even if you have reached the goal.

.. note:: **Assignment 1**

   Read these two papers in the given order.

   Rajpurkar, Pranav, et al. "Chexnet: Radiologist-level pneumonia detection on chest x-rays with deep learning." arXiv preprint arXiv:1711.05225 (2017).

   Strøm, Henrik, Steven Albury, and Lene Tolstrup Sørensen. "Machine Learning Performance Metrics and Diagnostic Context in Radiology." 2018 11th CMI International Conference: Prospects and Challenges Towards Developing a Digital Economy within the EU. IEEE, 2018.

   Reflect on the framing of the problem in the first paper.


What We Can And Can't Do With Machine Learning
----------------------------------------------

What is the difference between:

* Artificial Intelligence
* General Artificial Intelligence
* Machine Learning

.. figure:: _static/tasks.png
    :align: center
    :alt: xkcd tasks comic
    :figclass: align-center

    Machine Learning changed what we can do with computers.

Almost all practical applications of artificial intelligence has been found in machine learning, and especially in two areas of supervised machine learning: classification and regression.

.. note:: Read the paper Ng, Andrew. "What artificial intelligence can and can’t do right now." Harvard Business Review 9.11 (2016).

What is the point that Andrew Ng makes?

Does this align with your perceptions?


Supervised and Unsupervised Learning
------------------------------------

.. glossary::

    Supervised Learning
        In supervised learning we learn from labeled data.
        Supervised learning is a very powerful learning method, but it can be very costly to create labeled data sets.

    Unsupervised Learning
        In unsupervised learning we don't have labeled data, so we must learn about data points based on their relation to other data points.
        Unsupervised learning is open for a wider range of problems than supervised learning, but the insights we can gain are less powerfull.

Classification, Regression, and Clustering
------------------------------------------

.. glossary::

    Classification
        Classification problems are about labeling data, or assigning a category.
        As an example, we could have a classifier telling us if an image has a bird in it: *bird* and *non-bird* images.

    Regression
        Regression is about estimating continuous values, e.g. given a set of features about a house predict its price.

    Clustering
        Clustering is about a data point's relation (e.g. *distance*) to other data points, or the similarity between data points.

    Ranking
        Ordering of data points, finding best and worst sampels.

    Recommendations
        Based on rankings of samples, find sampels of interest.

    Anormaly Detection
        Find outliers, false data.

    Similarity Detection
        Ways of comparing samples and express their similarity by different metrics.
Reading List
------------

Strøm, Henrik. "Essay - Expert Perceptions of Artificial Intelligence" 2017. (course materials folder in repo)

`Tayo, Benjamin Obi. "Problem Framing: The Most Difficult Stage of a Machine Learning Project Workflow" <https://medium.com/towards-artificial-intelligence/problem-framing-the-most-difficult-stage-of-machine-learning-1a7f208ea414>`_

`Goldstein, AJ. "Deconstructing Data Science: Breaking The Complex Craft Into It’s Simplest Parts" <https://medium.com/the-mission/deconstructing-data-science-breaking-the-complex-craft-into-its-simplest-parts-15b15420df21>`_

Zhu, Yangyong, and Yun Xiong. "Defining data science." arXiv preprint arXiv:1501.05039 (2015).

`Tayo, Benjamin Obi. "Problem Framing: The Most Difficult Stage of a Machine Learning Project Workflow" <https://medium.com/towards-artificial-intelligence/problem-framing-the-most-difficult-stage-of-machine-learning-1a7f208ea414>`_
