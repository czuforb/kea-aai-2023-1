About the Applied Artificial Intelligence Course
================================================

.. _about:

Recommendations for students of this course
-------------------------------------------

This course is probably quite different from other courses you have taken so far, so please take note of the following:

.. warning:: It is strongly recommended that you attend all the lectures. Be here on time!

.. warning:: It is strongly recommended that you bring a notebook and pen for taking notes - and use them!

.. warning:: This course is 10 ECTS == 280 hours of workload - expect to use that amount of time!

Ways of communicating with the teacher
--------------------------------------

If you need to communicate with the lecturer please do so as follows:

* Preferred: talk to me in class
* Alternatively: contact me via Teams (don't forget to tag `@Henrik Strøm`) - preferably in public forum, as others might benefit from the information

Don't message us on Fronter - we will not be notified of messages, and you will be wasting your time.

Metadata
--------

.. csv-table:: Applied Artificial Intelligence Course
   :widths: 40, 40

   "Title", "Applied Artificial Intelligence"
   "Study Programme", "Software Development, Web Development"
   "Type of Education", "Full Time Education"
   "Level of Education", "Bachelor (top-up)"
   "Semester", "6"
   "Duration of the Module", "1 semester or 16 weeks"
   "ECTS", "10"
   "Expected Workload", "280 hours"
   "Programme Elemements", "Elective"
   "Language", "English"
   "Start Time", "Spring and Autum"
   "Location", "Guldbergsgade 29N, København N"
   "Responsible for the Module", "Henrik Strøm"


Objectives
----------

The objective of the module is to qualify the student to apply methods from Artificial Intelligence, Machine Learning, and Data Science in a structured manner, extract inferential knowledge from a dataset, and make probabilistic forecasts using state of the art models.

In addition, the student must be able to report on the findings and make use of visualisation.

Knowledge
---------

The objective is to give the student knowledge of:

- various definitions of the fields of Artificial Intelligence, Machine Learning, and Data Science
- what is possible and not possible using Artificial Intelligence and Machine Learning
- designing experiments for data collection
- using a framework for numerical computations
- using a framework for general data analysis
- using a framework for descriptive and inferential statistical analysis
- building models to “predict the future”
- a variety of data analysis algorithms and their applications
- writing and accessing scientific literature


The objective is that the student will have acquired the ability to:

- collect and obtain data from a variety of sources
- organise data to prepare for analysis
- explore data to gain insights
- apply basic descriptive and inferential statistics
- make predictions using Artificial Intelligence and Machine Learning
- use state of the art methods to develop highly convincing and trustworthy arguments communicate findings in a written report, using visualisations

Proficiencies
-------------

The objective is that the student will have acquired proficiency in participating and contributing in projects on Artificial Intelligence, Machine Learning, and Data Science.

Type of Instruction
-------------------

The teaching is organised as a variation between class teaching, guest lecturing, company visits, group project work and individual work. The learning is most often problem-based and cross-disciplinary and always practise-oriented. In addition to learning the subject, the student will gain the competences to work individually and in collaboration with others.

The common aim of the activities is to enable students to work with abstract problems in Artificial Intelligence, Machine Learning, and Data Science.

Mandatory Hand-Ins
------------------

The student must turn in the two mandatory assignments and have them approved to attend the exam for this course.

Both mandatory assignments have an element of peer-review. The project can't be approved without both elements completed.

The second mandatory assignment is the foundation for your exam project - this means that you will have feedback on your work, and will be able to make improvements.

Exam
----

30 minutes oral exam based on project hand in and the course curriculum in general:

* 10 minutes for your presentation
* 15 minutes for questions
* 5 minutes for grading

