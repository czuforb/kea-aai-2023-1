Assignments
===========

.. _assignment1:

Assignment 1
------------

The these two papers in the given order.

Rajpurkar, Pranav, et al. "Chexnet: Radiologist-level pneumonia detection on chest x-rays with deep learning." arXiv preprint arXiv:1711.05225 (2017).

Strøm, Henrik, Steven Albury, and Lene Tolstrup Sørensen. "Machine Learning Performance Metrics and Diagnostic Context in Radiology." 2018 11th CMI International Conference: Prospects and Challenges Towards Developing a Digital Economy within the EU. IEEE, 2018.

Reflect on the framing of the problem in the first paper.

.. _assignment2:

Assignment 2
------------

Read the paper by Nunamaker.

Nunamaker Jr, Jay F., Minder Chen, and Titus DM Purdin. "Systems development in information systems research." Journal of management information systems 7.3 (1990): 89-106.

In your groups, write a summary and review of the Nunamaker paper (1 to 2 pages).

This is not a Data Science paper, so the structure discussed in Writing Papers is not a good structure.
Make sure to use proper referencing.

Assignment 3
------------

In groups: find five examples of each of the four data types: nominal, ordinal, interval, and ratio.
Explain your reasoning.

Assignment 4
------------

Find a data set and explore its data.

What kind of questions do you think you could answer by analyzing this data set?

Assignment 5
------------

Read `5 Quick and Easy Data Visualizations in Python with Code <https://towardsdatascience.com/5-quick-and-easy-data-visualizations-in-python-with-code-a2284bae952f>`_.

Try to run the scripts in a Jupyter notebook and play around with the scripts.

Assignment 6
------------

Do a peer-review of two other groups' hand in of assignment 2.

If you are in group 1 you will review group 2 and 3, group 2 will review 3 and 4 ... group 8 will review 1 and 2.

Assignment 7
------------

Try change the hyper parameters for our first gradient descent implementation, and try to answer these questions:

- How many iterations are necessary to get a good result?
- What is the smallest and largest values of alpha that works well?
- What influence does different values of theta have?

Assignment 8
------------

How many iterations are necessary to get a good approximation?

How much bigger is the error at that point than doing 5000 iterations?

Assignment 9
------------

Our second implementation supports a `verbose` mode that allows you to observe gradual changes to the model during optimization.

The problem is that the verbose mode is too verbose - it prints out thousands of lines of text optimizing a simple model.

Add a parameter `verbose_interval` that will make the verbose mode less verbose.
A value of 100 should print for every 100 optimizations etc.

Try to do an optimization where you get 10 prints, and plot the suboptimal intermediate results in a figure.

Assignment 10
-------------

Find a data set of two variables that are linearly dependent.
The data set should have tens of thousands of samples or more.

Compare the two gradient descent implementations.
How do the two implementations compare in computation time performance?

Assignment 11
-------------

Use SciKit Learn to make a model for the data set you worked on in Assignment 10.

Assignment 12
-------------

Explore sklearn for other classifier models.
Read their documentation and try to train them for the XOR problem.

*PS: Take care using the correct models: you can't use classification models for regression and vice versa.*

Assignment 13
-------------

sklearn comes with a few data sets ready for machine learning.

Take a look at the documentation: `Dataset loading utilities <https://scikit-learn.org/stable/datasets/index.html>`_

Explore the data sets and try to make predictions using some of the models you found in assignment 12.

*PS: Take care using the correct models: you can't use classification models for regression and vice versa.*

Assignment 14
-------------

Experiment with the code given in the Week 16 lecture for training models on the MNIST data set.

Try to find some good hyper-parameters for the various models.
You should use the sklearn documentation to help you understand the hyper-parameters.

Focus on the Random Forest and MLP models.

Assignment 15
-------------

Try to apply the various performance metrics given in the *evaluate* function in the Week 16 lecture.

Having read the Sokolova paper, how do you interpret the results?

Assignment 16
-------------

The MNIST data set is not structured ideally for machine learning, in that there is only one output value for ten labels.

Restructure the data set so you have ten output values, one for each label, that is either 0 or 1.

Train a few models with this refined data set - can you get better results?

Assignment 17
-------------

With the data set from Mandatory Assignment 1, try to apply Principal Components Analysis so you can visualize the data in a 2D scatterplot.

You have two dimensions available *(x, y)* in the scatter plot, but you can also use color for one (target) feature. You could choose attendance for the target feature (plot 1).

Now apply the proper clustering analysis to the problem.
How many clusters do you end up with?
Try to plot the data with the cluster as the color (plot 2).

