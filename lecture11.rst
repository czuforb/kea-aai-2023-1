Lecture 11
==========

Analyzing Text
--------------

In this lecture we will be looking at text analysis, or *Natural Language Processing*.

The lecture consists of three parts:

- a video lecture by Alice Zhao from `A Dash of Data <http://adashofdata.com>`_
- work through Alice Zhao's Jupyter notebooks
- an extra assignment

I suggest you watch all the video lectures to begin with, while taking notes.

Video Lectures
--------------

Part 1: Introduction to NLP & Data Science
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|

.. raw:: html

   <iframe width="744" height="400" src="https://www.youtube.com/embed/5BVebXXb2o4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

Part 2: Data Cleaning & Text Pre-Processing in Python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|

.. raw:: html

   <iframe width="744" height="400" src="https://www.youtube.com/embed/iQ1bfDMCv_c" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

Part 3: Exploratory Data Analysis & Word Clouds in Python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|

.. raw:: html

   <iframe width="744" height="400" src="https://www.youtube.com/embed/VraAbgAoYSk" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

Part 4: Sentiment Analysis with TextBlob in Python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|

.. raw:: html

   <iframe width="744" height="400" src="https://www.youtube.com/embed/N9CT6Ggh0oE" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

Part 5: Topic Modeling with Latent Dirichlet Allocation in Python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|

.. raw:: html

   <iframe width="744" height="400" src="https://www.youtube.com/embed/NYkbqzTlW3w" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

Part 6: Text Generation with Markov Chains in Python
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

|

.. raw:: html

   <iframe width="744" height="400" src="https://www.youtube.com/embed/MGVdu39gT6k" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

The Jupyter Notebooks
---------------------

You should start by installing these packages into the Python environment that you use for Data Science:

.. code-block:: text

   bs4
   wordcloud
   textblob
   gensim

Once installed, clone this git repository: `https://github.com/adashofdata/nlp-in-python-tutorial <https://github.com/adashofdata/nlp-in-python-tutorial>`_

Now go through the Jupyter notebooks.
I recommend that you experiment along the way.

Lemmatization and Stemming
---------------------------

There are two key concepts in NLP not addressed (but mentioned) in the videos above.

Lemmatization and stemming are special cases of normalization.
They identify a canonical representative for a set of related word forms.

Let's see an example:

.. raw:: html

   <iframe src="_static/lemma_stemming.html" width="700px" height="500px"></iframe>

As you can see we get a simpler form of the words, many of these representations are the same.
This is very often used in NLP to help simplify a corpus and make it easier to analyze.

Lemmatization or stemming can be applied at an early state in the process.
You can try to look at the notebooks and analyze where you would apply this process.

Lemmatization and stemming is also widely used in search and databases, because it can greatly enhance the chance of finding a good match for a word.

Extra Assignment
----------------

Run a new analysis on these transcripts:

- George Carlin: Dumb Americans (2006)
- Chris Rock: Never Scared (2004)
- Denis Leary: No Cure for Cancer (1993)
- Jimmy Carr: The Best of Ultimate Gold Greatest Hits (2019)

You can add more if you like.

This material is quite different.
You should go through the whole process from asking a research question.
