# Geron on Github

You can find notebooks for the Geron chapters on Github via the link below:

[https://github.com/ageron/handson-ml](https://github.com/ageron/handson-ml)
