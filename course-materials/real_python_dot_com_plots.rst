.. code:: ipython3

    import pandas as pd

.. code:: ipython3

    download_url = ("https://raw.githubusercontent.com/fivethirtyeight/"
      "data/master/college-majors/recent-grads.csv" )

.. code:: ipython3

    df = pd.read_csv(download_url)

.. code:: ipython3

    type(df)




.. parsed-literal::

    pandas.core.frame.DataFrame



.. code:: ipython3

    pd.set_option("display.max.columns", None)

.. code:: ipython3

    df.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Rank</th>
          <th>Major_code</th>
          <th>Major</th>
          <th>Total</th>
          <th>Men</th>
          <th>Women</th>
          <th>Major_category</th>
          <th>ShareWomen</th>
          <th>Sample_size</th>
          <th>Employed</th>
          <th>Full_time</th>
          <th>Part_time</th>
          <th>Full_time_year_round</th>
          <th>Unemployed</th>
          <th>Unemployment_rate</th>
          <th>Median</th>
          <th>P25th</th>
          <th>P75th</th>
          <th>College_jobs</th>
          <th>Non_college_jobs</th>
          <th>Low_wage_jobs</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1</td>
          <td>2419</td>
          <td>PETROLEUM ENGINEERING</td>
          <td>2339.0</td>
          <td>2057.0</td>
          <td>282.0</td>
          <td>Engineering</td>
          <td>0.120564</td>
          <td>36</td>
          <td>1976</td>
          <td>1849</td>
          <td>270</td>
          <td>1207</td>
          <td>37</td>
          <td>0.018381</td>
          <td>110000</td>
          <td>95000</td>
          <td>125000</td>
          <td>1534</td>
          <td>364</td>
          <td>193</td>
        </tr>
        <tr>
          <th>1</th>
          <td>2</td>
          <td>2416</td>
          <td>MINING AND MINERAL ENGINEERING</td>
          <td>756.0</td>
          <td>679.0</td>
          <td>77.0</td>
          <td>Engineering</td>
          <td>0.101852</td>
          <td>7</td>
          <td>640</td>
          <td>556</td>
          <td>170</td>
          <td>388</td>
          <td>85</td>
          <td>0.117241</td>
          <td>75000</td>
          <td>55000</td>
          <td>90000</td>
          <td>350</td>
          <td>257</td>
          <td>50</td>
        </tr>
        <tr>
          <th>2</th>
          <td>3</td>
          <td>2415</td>
          <td>METALLURGICAL ENGINEERING</td>
          <td>856.0</td>
          <td>725.0</td>
          <td>131.0</td>
          <td>Engineering</td>
          <td>0.153037</td>
          <td>3</td>
          <td>648</td>
          <td>558</td>
          <td>133</td>
          <td>340</td>
          <td>16</td>
          <td>0.024096</td>
          <td>73000</td>
          <td>50000</td>
          <td>105000</td>
          <td>456</td>
          <td>176</td>
          <td>0</td>
        </tr>
        <tr>
          <th>3</th>
          <td>4</td>
          <td>2417</td>
          <td>NAVAL ARCHITECTURE AND MARINE ENGINEERING</td>
          <td>1258.0</td>
          <td>1123.0</td>
          <td>135.0</td>
          <td>Engineering</td>
          <td>0.107313</td>
          <td>16</td>
          <td>758</td>
          <td>1069</td>
          <td>150</td>
          <td>692</td>
          <td>40</td>
          <td>0.050125</td>
          <td>70000</td>
          <td>43000</td>
          <td>80000</td>
          <td>529</td>
          <td>102</td>
          <td>0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>5</td>
          <td>2405</td>
          <td>CHEMICAL ENGINEERING</td>
          <td>32260.0</td>
          <td>21239.0</td>
          <td>11021.0</td>
          <td>Engineering</td>
          <td>0.341631</td>
          <td>289</td>
          <td>25694</td>
          <td>23170</td>
          <td>5180</td>
          <td>16697</td>
          <td>1672</td>
          <td>0.061098</td>
          <td>65000</td>
          <td>50000</td>
          <td>75000</td>
          <td>18314</td>
          <td>4440</td>
          <td>972</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    %matplotlib


.. parsed-literal::

    Using matplotlib backend: MacOSX


.. code:: ipython3

    %matplotlib inline

.. code:: ipython3

    df.plot(x="Rank", y=["P25th", "Median", "P75th"])




.. parsed-literal::

    <AxesSubplot:xlabel='Rank'>




.. image:: output_8_1.png


.. code:: ipython3

     import matplotlib.pyplot as plt

.. code:: ipython3

    plt.plot(df["Rank"], df["P75th"])




.. parsed-literal::

    [<matplotlib.lines.Line2D at 0x7fa06da3fb80>]




.. image:: output_10_1.png


.. code:: ipython3

    plt.plot(df["Rank"],df["P25th"], df["P75th"])




.. parsed-literal::

    [<matplotlib.lines.Line2D at 0x7fa06d99f4f0>,
     <matplotlib.lines.Line2D at 0x7fa06d99f520>]




.. image:: output_11_1.png


.. code:: ipython3

    median_column = df["Median"]

.. code:: ipython3

     median_column.plot(kind="hist")




.. parsed-literal::

    <AxesSubplot:ylabel='Frequency'>




.. image:: output_13_1.png


.. code:: ipython3

    unemployment_col = df["Unemployment_rate"]

.. code:: ipython3

    type(unemployment_col)




.. parsed-literal::

    pandas.core.series.Series



.. code:: ipython3

    unemployment_col.plot(kind="hist")




.. parsed-literal::

    <AxesSubplot:ylabel='Frequency'>




.. image:: output_16_1.png


.. code:: ipython3

    top_5 = df.sort_values(by="Median", ascending=False).head()

.. code:: ipython3

    top_5.plot(x="Major", y="Median", kind="bar", rot=5, fontsize=4)




.. parsed-literal::

    <AxesSubplot:xlabel='Major'>




.. image:: output_18_1.png


.. code:: ipython3

    #create new data frame for high earners
    top_medians = df[df["Median"] > 60000].sort_values("Median")

.. code:: ipython3

    #can then plot a compound bar chart for these high salary majors
    top_medians.plot(x="Major", y=["P25th", "Median", "P75th"], kind="bar")




.. parsed-literal::

    <AxesSubplot:xlabel='Major'>




.. image:: output_20_1.png


.. code:: ipython3

    df.plot(x="Median", y="Unemployment_rate", kind="scatter")




.. parsed-literal::

    <AxesSubplot:xlabel='Median', ylabel='Unemployment_rate'>




.. image:: output_21_1.png


.. code:: ipython3

    med_unem = df[["Median", "Unemployment_rate"]]

.. code:: ipython3

    #can check correlation 0..1 - = negative correlation
    med_unem.corr(method='pearson')




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Median</th>
          <th>Unemployment_rate</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>Median</th>
          <td>1.000000</td>
          <td>-0.108833</td>
        </tr>
        <tr>
          <th>Unemployment_rate</th>
          <td>-0.108833</td>
          <td>1.000000</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    for colname in df.columns:
        print(colname)


.. parsed-literal::

    Rank
    Major_code
    Major
    Total
    Men
    Women
    Major_category
    ShareWomen
    Sample_size
    Employed
    Full_time
    Part_time
    Full_time_year_round
    Unemployed
    Unemployment_rate
    Median
    P25th
    P75th
    College_jobs
    Non_college_jobs
    Low_wage_jobs


.. code:: ipython3

    list(df.columns)




.. parsed-literal::

    ['Rank',
     'Major_code',
     'Major',
     'Total',
     'Men',
     'Women',
     'Major_category',
     'ShareWomen',
     'Sample_size',
     'Employed',
     'Full_time',
     'Part_time',
     'Full_time_year_round',
     'Unemployed',
     'Unemployment_rate',
     'Median',
     'P25th',
     'P75th',
     'College_jobs',
     'Non_college_jobs',
     'Low_wage_jobs']



.. code:: ipython3

    cat_totals = df.groupby("Major_category")["Total"].sum().sort_values()

.. code:: ipython3

    cat_totals




.. parsed-literal::

    Major_category
    Interdisciplinary                        12296.0
    Agriculture & Natural Resources          75620.0
    Law & Public Policy                     179107.0
    Physical Sciences                       185479.0
    Industrial Arts & Consumer Services     229792.0
    Computers & Mathematics                 299008.0
    Arts                                    357130.0
    Communications & Journalism             392601.0
    Biology & Life Science                  453862.0
    Health                                  463230.0
    Psychology & Social Work                481007.0
    Social Science                          529966.0
    Engineering                             537583.0
    Education                               559129.0
    Humanities & Liberal Arts               713468.0
    Business                               1302376.0
    Name: Total, dtype: float64



.. code:: ipython3

    cat_totals.plot(kind='barh', fontsize=4)




.. parsed-literal::

    <AxesSubplot:ylabel='Major_category'>




.. image:: output_28_1.png


.. code:: ipython3

    # Pie chart is hard to read because business degrees dominate with relatively few
    # interdisciplinary degrees
    
    cat_totals.plot(kind="pie")




.. parsed-literal::

    <AxesSubplot:ylabel='Total'>




.. image:: output_29_1.png


.. code:: ipython3

    #can put categories into "bins"
    small_cat_totals = cat_totals[cat_totals < 100_000]
    big_cat_totals = cat_totals[cat_totals > 100_000]
    
    # put the small cat totals together in a category called "other"
    small_sums = pd.Series([small_cat_totals.sum()], index=["Other"])

.. code:: ipython3

    # add this back to the big_cat_totals series to get all the info
    # back in one series but now all the small ones are grouped under "other"
    
    big_cat_totals = big_cat_totals.append(small_sums)
    big_cat_totals.plot(kind="pie", label="")




.. parsed-literal::

    <AxesSubplot:>




.. image:: output_31_1.png


.. code:: ipython3

    # better as no overlapping text 
    
    
    # bar chart still clearer IMHO!
    big_cat_totals.plot(kind="barh", label="", fontsize=4)




.. parsed-literal::

    <AxesSubplot:>




.. image:: output_32_1.png


.. code:: ipython3

    # within categories can check the distribution and density 
    df[df["Major_category"] == "Engineering"]["Median"].plot(kind="hist")




.. parsed-literal::

    <AxesSubplot:ylabel='Frequency'>




.. image:: output_33_1.png


