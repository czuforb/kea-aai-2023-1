Lecture 15
==========


Deep Neural Networks
--------------------

In this lecture we are going to look at Deep Neural Networks.

You have already met the Multilayer Perceptron - Deep Neural Networks are very much the same thing, but we use the term Deep Neural Networks for a wider variety of models.

As you already know many of the concepts, we will spend most of the time looking at specific examples.

.. note:: You are encouraged to play with these examples as much as possible, and also try go through the process using other datasets.


Moons
~~~~~

The first example is based on the `make_moons` dataset from `sklearn`.
However, we will be using `Tensorflow` via the `Keras` interface to get more control of our model than we are able to using `SciKit Learn`.

Here's a link to `make_moons`:

https://scikit-learn.org/stable/modules/generated/sklearn.datasets.make_moons.html


.. raw:: html

   <iframe src="_static/working_through_moons.html" width="700px" height="500px"></iframe>

During the example I am making use of a `utils.py` file.
The file is listed below.

.. code-block:: python

    import time
    import matplotlib.pyplot as plt
    import numpy as np
    import keras


    def plot_decision_boundary(X, y, model, steps=1000, cmap='Paired'):
        cmap = plt.get_cmap(cmap)

        # Define region of interest by data limits
        xmin, xmax = X[:,0].min() - 1, X[:,0].max() + 1
        ymin, ymax = X[:,1].min() - 1, X[:,1].max() + 1
        steps = 1000
        x_span = np.linspace(xmin, xmax, steps)
        y_span = np.linspace(ymin, ymax, steps)
        xx, yy = np.meshgrid(x_span, y_span)

        # Make predictions across region of interest
        labels = model.predict(np.c_[xx.ravel(), yy.ravel()])

        # Plot decision boundary in region of interest
        z = labels.reshape(xx.shape)

        fig, ax = plt.subplots()
        ax.contourf(xx, yy, z, cmap=cmap, alpha=0.5)

        # Get predicted labels on training data and plot
        train_labels = model.predict(X)
        ax.scatter(X[:,0], X[:,1], c=y, cmap=cmap, lw=0)

        return fig, ax


    class Timer:
        def __init__(self):
            self.time = None

        def __enter__(self):
            self.time = time.time()

        def __exit__(self, exc_type, exc_value, exc_traceback):
            time_passed = time.time() - self.time
            print(f'** {time_passed:.2f} seconds.')


.. note:: **Assignment:** We managed to do pretty well on the Moons dataset. However, we did not pay attention to training time.

    Can we shorten the training time by making changes to the model? You obviously need to run the examples on your own machine in order to compare the trainig times.

    As a competition: who can come up with the biggest difference (in percentage) in training time compared to the examples, without sacrificing performance. If you allow 5% worse performance, can you do it even better then?


MNIST
~~~~~

Now it's time for you to get your hands dirty.
Work your way through this example from the Tensorflow website.

https://www.tensorflow.org/tutorials/quickstart/beginner


.. warning:: Remember - don't rush through these examples. Play around with things, and look up things you forgot or don't understand.


.. note:: If you are the lucky owner of a computer with a CUDA enabled GPU, that would be an nVIDIA GPU, you can speed things up significantly by installing the correct version of Tensorflow. Some minor code changes are needed to enable the GPU, but it should all be well documented.

    You might also be the lucky owner of an Apple computer with an M1 or M2 processor. In this case you can install Tensorflow according to this guide to speed things up: https://developer.apple.com/metal/tensorflow-plugin/
    No code changes are needed for the Apple GPU.
    Unfortunately, it's not worth the trouble.
    Because the current plugin only supports a single GPU, it's much faster running in normal CPU mode.
