Lecture 13
==========

Cluster Analysis
----------------

Cluster analysis is part of unsupervised learning.
In cluster analysis, we try to find samples that are similar to each other.

K-Means
-------

K-Means is a *clustering algorithm*, that means, it can split a training set :math:`X` into :math:`K` clusters, or as Pena et al. puts it, "the fundamental data clustering problem may be defined as grouping similar objects together".

Pena, José M., Jose Antonio Lozano, and Pedro Larranaga. "An empirical comparison of four initialization methods for the k-means algorithm." Pattern recognition letters 20.10 (1999): 1027-1040.

Clustering (in unsupervised learning) is very similar to the problem of classification in supervised learning.
The K-Means algorithm can be applied to parametric (interval and ration) data.


Pena et al. describes the algorithm in detail.
The data set is initially split into :math:`K` clusters, and the *cluster centroids* are calculated for each cluster, as a feature vector with each feature set as the mean of that feature in the data set. Then, for each sample, the euclidean distance (distance function) to each cluster centroid is calculated, and the sample assigned to the centroid with the smallest error. This process repeats until convergence.

Pena et al. discusses various initializations of the algorithm, but end up concluding that the usual random initialization perform quite well.

The K-Means algorithm is widely used and is considered a very well performing algorithm.

Here is a more easy-to-understand explanation of K-Means: `K-Means Clustering: From A-Z <https://towardsdatascience.com/k-means-clustering-from-a-to-z-f6242a314e9a>`_


K-Modes
-------

*K-Modes* is very similar to the K-Means algorithm, but whereas the K-Means algorithm deal with parametric data, K-Modes deal with non-parametric data.
The need for clustering of non-parametric data come from the world of data mining, where we often find categorical data such as :math:`Gender \in \{Male, Female \}`.

Where the K-Means algorithm calculates the centroids based on means, K-Modes uses the mode, and a *similarity measure* is used as a distance function:

.. math::

    d(X, Y) = \sum_{j=1}^{m} \delta(x_j, y_j) \\
    \text{where} \\ \delta(x_j, y_j) =
    \begin{cases}
        0 \enspace \text{for} \enspace x_j = y_j \\
        1 \enspace \text{for} \enspace x_j \neq y_j
    \end{cases}

You can see an implementation of K-Modes below:

.. code-block:: python

    def k_modes(X, k):
        m, n = np.shape(X) # m samples, n features
        # random cluser centers for each of the k clusters
        kc = X[np.random.choice(range(m), k, replace=False)]
        # cluster assignment array for each sample
        sample_assignments = np.zeros(m, dtype=int)

        while True:
            last_kc = "".join(str(e) for e in kc)
            # assign samples to the mode with the smallest dist
            for i in range(m):
                shortest = (0, Inf) # index and dissimilarity
                # distance to each cluster center
                for j in range(k):
                    d = dissimilarity(kc[j], X[i])
                    if d < shortest[1]:
                        shortest = (j, d)
                sample_assignments[i] = shortest[0]
            # assign new cluster centers
            for i, cluster in enumerate(kc):
                # the X subset assigned to kc[i]
                sub_X = X[(sample_assignments == i)].T
                for j, feature in enumerate(sub_X):
                    kc[i, j] = mode(feature)[0][0]
            if last_kc == "".join(str(e) for e in kc):
                break
        return kc, sample_assignments


Distance Measures
-----------------

When dealing with clustering, we often have to look into *distance measures*.
In this section we will look at a few different distance measures.


Euclidean Distance
~~~~~~~~~~~~~~~~~~

In daily life, we are used to dealing with *Euclidean* distances.
However, this is not the only way to measure distances.

The Euclidean distance between two points `x` and `y` with `n` features is given:

.. math::

    D(x, y) = \sum_{i=1}^{n} \sqrt{ (x_i-y_i)^2 }


Cosine Distance
~~~~~~~~~~~~~~~

Cosine distance measures the distance by angle.

To calculate the cosine distance we must first find the *cosine similarity*:


.. math::

    \frac{\sum_{i=1}^{n} x_iy_i }{\sqrt{\sum_{i=1}^{n}x_i^2}\sqrt{\sum_{i=1}^{n}{y_i^2}}}

Once we have the cosine similarity, the *cosine distance* is given:

.. math::

    Cosine Distance = 1 - Cosine Similarity


Hamming Distance
~~~~~~~~~~~~~~~~

Hamming distance is widely used when dealing with text.
In hamming distance, we simply compare elements of two equal size vectors (or strings) and count the number of differences.

.. math::

    d(X, Y) = \sum_{j=1}^{m} \delta(x_j, y_j) \\
    \text{where} \\ \delta(x_j, y_j) =
    \begin{cases}
        0 \enspace \text{for} \enspace x_j = y_j \\
        1 \enspace \text{for} \enspace x_j \neq y_j
    \end{cases}

As you might have noticed, this is identical to the distance measure we used for the *K-Mode* implementation earlier in this lecture.


Other Distance Measures
~~~~~~~~~~~~~~~~~~~~~~~

There are many other distance measures that each have their applications; to mention some of the most important:

- Damerau-Lenshtein distance
- Gray code
- Manhattan distance
- Sørensen similarity index

And many other.

The idea here is not to give an exhaustive list, but to explore the idea that many different distance measures exist, and each have their own applications.

.. note::

    When you are studying an field such as artificial intelligence, you will often find yourself faced with sub-fields that seems like a whole new area of study. You will never be able to cover all these sub-fields, but knowing that they exist will allow you to explore them when needed.

To learn more read this:
https://towardsdatascience.com/9-distance-measures-in-data-science-918109d069fa


Exploring Cluster Algorithms
----------------------------

Just as we have many different distance measures, we also have many different clustering algorithms.

Without going into too much detail as to how each one works, let's try to explore some of them.

.. raw:: html

   <iframe src="_static/clusters.html" width="700px" height="500px"></iframe>


Assignment
----------

Using the *Clustering* notebook above as inspiration, systematically try a set of datasets with different characteristics:

- overlaps vs no overlap
- linear vs non-linear

Try the different clustering algorithms with different configurations, and try to make a list of what each of these models are good at, and what they are not good at.
