Mandatory Assignments & Exam Project
====================================


Both mandatory assignments (in all its parts) must be approved to have access to the exam.


Mandatory Assignment 1
----------------------


TBD

..
    For your Mandatory Assignment 1 you must do the following:

    - frame a relevant problem
    - phrase a hypothesis and a corresponding null hypothesis
    - phrase a research question
    - find one or more datasets to support your research
    - do a relevant statistical analysis
    - create two or more predictive models
    - fine tune your models
    - write a max two page article (in Latex) with relevant visuals

    Submit the following to the assignment repository:

    - article in PDF
    - all project files, including notebooks, `.tex` file, etc.

    Follow the directions for submission in the assignments repository `README.md` file.

    Your submission should not include large datasets, but clearly document how to obtain the files.

    Part 1: project as listed above, due **10th October 2022**.

    Part 2: peer review of two other groups, due **17th October 2022**.


Mandatory Assignment 2 & Exam Project
-------------------------------------

TBD

..
    For your Mandatory Assignment 2 and Exam Project you must do the following:

    - frame a relevant problem
    - phrase a hypothesis and a corresponding null hypothesis
    - phrase a research question
    - find one or more datasets to support your research
    - do a relevant statistical analysis
    - create three or more predictive models
    - fine tune your models
    - write a max five page article (in Latex) with relevant visuals

    The exam project is a continuation of Mandatory Assignment 2.
    Getting feed-back on Mandatory Assignment 2 will help you improve your Exam Project.


Mandatory Assignment 2
~~~~~~~~~~~~~~~~~~~~~~


TBD

..
    Submit the following to the assignment repository:

    - article in PDF
    - all project files, including notebooks, `.tex` file, etc.

    Follow the directions for submission in the assignments repository `README.md` file.

    Your submission should not include large datasets, but clearly document how to obtain the files.

    Part 1: project as listed above, due **8th December 2022**.

    Part 2: peer review of two other groups, due **12th December 2022**.


Exam Project
~~~~~~~~~~~~


TBD

..
    Submit the following to Wiseflow as directed by the Exam Office:

    - article in PDF
    - all project files, including notebooks, `.tex` file, etc.

    Project files can be uploaded in a standard **ZIP** file.

    Your submission should not include large datasets, but clearly document how to obtain the files.

