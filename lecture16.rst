Lecture 16
==========


In this lecture we will look at more advanced neural network, by introducing a few new layer types.

These so-called Convolutional Neural Networks (CNN) work extremely well with image processing, but they are not general purpose layers.


Convolutional Neural Networks
-----------------------------

Let's start by taking a look at the basic architectural idea of a CNN.

.. figure:: _static/convolutional_neural_network.png
    :align: center
    :alt: Basick CNN architecture.
    :figclass: align-center

    Basic CNN architecture.


The network architecture is segmented into two major parts: the *feature extraction* part, and the *classification* part.

The feature extraction part is made up of *convolution* and *max pooling* layers.


.. figure:: _static/conv2d.gif
    :align: center
    :alt: Convolutional 2D layer.
    :figclass: align-center

    Convolutional 2D layer.


Parsing the *kernel* over the input image, or more generally the prior layer, computes the output image.

The computed output value if image kernel *K* over partial image *I* is:

.. code:: python

    np.sum(I * K)

The kernel gets updated during training, and the different convolutions will get trained in extracting different features, due to the random initialization.
Some kernels may detect horizontal edges, other vertical edges, some much more complex features.
The deeper into the network you get, the more complex the features.

To capture the dominant *signal* in an output image, convolutional layers are most often combined with *max pooling* layers.

Max pooling layers extracts the maximum values, i.e., the dominant part of the signal, via simple filtering as shown below.


.. figure:: _static/maxpooling2d.png
    :align: center
    :alt: Max Pooling 2D layer.
    :figclass: align-center

    Max Pooling 2D layer.


With this, we can take a look at our first CNN.


MNIST Classification in Tensorflow
----------------------------------

.. raw:: html

   <iframe src="_static/tf-mnist.html" width="700px" height="500px"></iframe>

