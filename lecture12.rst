Lecture 12
==========


Dimensionality Reduction
------------------------

In this lecture we will look at dimensionality reduction.
Dimensionality reduction is a kind of *unsupervised learning*, meaning we don't have a labeled data set.


Principal Component Analysis
----------------------------

Principal Component Analysis (PCA) is a technique used for dimensionality reduction.
When working with machine learning we often have data sets containing hundreds, thousands or even more dimensions.
This can be a challenge, and this is where PCA enters the picture.

There are two main uses of dimensionality reduction: the first is in visualization.
We can only really display two dimensions on screens and paper, so any visualization of high-dimensional data need to be fitted to two dimensions.

The other use of dimensionality reduction is in compression, or rather reduction.
We might sometime choose to combine a number of features when training a model to reduce data size and computation time.

PCA is one of the most widely applied techniques for dimensionality reduction.

.. note:: Before applying PCA, the data must be standardized and scaled. You can use the `StandardScaler` from `sklearn.preprocessing` for this.

PCA is done by first computing the covariance matrix :math:`\Sigma` (that is Sigma, not sum):

.. math::

   \Sigma = \frac{1}{m}X^T X

Where :math:`X` is the data set with :math:`m` samples.

Having the covariance matrix we can now use *singular value decomposition* to compute the principal components :math:`U`:

.. code-block:: python

   U, S, V = np.linalg.svd(Sigma)

Now we can compute the projection :math:`Z` of :math:`X` onto :math:`k` dimensions:

.. math::

   Z = XU_k

:math:`U_k` is the first :math:`k` columns of the matrix :math:`U`.

Let's make a quick implementation:

.. code-block:: python

   import numpy as np


   def pca(X, k):
      m = X.shape[0]
      Sigma = (1/float(m)) * (X.T.dot(X))
      U, S, V = np.linalg.svd(Sigma)
      U = U[:, 0:k]
      Z = X.dot(U)
      return Z

You can obviously find an implementation of PCA in scikit-learn.

.. note:: It is important to notice that the reduced features you get when applying PCA is in a *latent space* - meaning, you can't directly interpret the individual features anymore. The features only have meaning as a collection of features.

In the following example you can see a PCA projection of the Iris dataset.

First, we apply a `StandardScaler` to get scaled and standardized data (zero mean, standard deviation 1).
We then project onto two dimensions, and then reconstruct the data via an inverse transform.

.. code-block:: python

    import numpy as np
    from sklearn.datasets import load_iris
    from sklearn.decomposition import PCA
    from sklearn.preprocessing import StandardScaler

    np.set_printoptions(precision=3)

    X, _ = load_iris(return_X_y=True)
    scaler = StandardScaler()
    X_scaled = scaler.fit_transform(X)

    pca = PCA(2)
    X_pca = pca.fit_transform(X_scaled)

    X_orig = np.dot(X_pca, pca.components_)
    X_orig_backscaled = scaler.inverse_transform(X_orig)

    print(f"                    Original: {X[0]}")
    print(f"                      Scaled: {X_scaled[0]}")
    print(f"                   PCA space: {X_pca[0]}")
    print(f"           Original from PCA: {X_orig[0]}")
    print(f"Original from PCA backscaled: {X_orig_backscaled[0]}")

.. code-block:: text

                        Original: [5.1 3.5 1.4 0.2]
                          Scaled: [-0.901  1.019 -1.34  -1.315]
                       PCA space: [-2.265  0.48 ]
               Original from PCA: [-0.999  1.053 -1.303 -1.247]
    Original from PCA backscaled: [5.019 3.515 1.466 0.252]


Notice how the original data differs slightly from the reconstructed data.
We are obviously loosing information when applying PCA, as can be seen observing the variance in the original dataset `X` versus the retained variance in the PCA model.

.. code-block:: python

    sum(pca.explained_variance_)

.. code-block:: text

    3.858249954362484

.. code-block:: python

    np.var(X)

.. code-block:: text

    3.896056416666667

.. code-block:: python

    np.var(X) - sum(pca.explained_variance_)

.. code-block:: text

    0.037806462304183164


In the next example we can see how PCA can reduce the Iris dataset for the purpose of visualization.

.. raw:: html

   <iframe src="_static/pca-iris.html" width="700px" height="500px"></iframe>


While still useful, going from very high dimensional data, like MNIST with 784 dimensions, we obviously lose out quite a lot, as you can see in the next example.

.. raw:: html

   <iframe src="_static/pca-mnist-plot.html" width="700px" height="500px"></iframe>


Let's look a bit more into the loss of variance with the next example.

.. raw:: html

   <iframe src="_static/pca-mnist.html" width="700px" height="500px"></iframe>


Finally, let's take a visual look at the degradation in image quality, as a result of PCA reduction.

.. raw:: html

   <iframe src="_static/pca-mnist-reconstruct.html" width="700px" height="500px"></iframe>
