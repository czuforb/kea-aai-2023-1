Lecture 10
==========

Predictive Models
-----------------

Models
------

In Data Science, we have several different families of models.
In this lecture we are going to look at some of the most powerful models we have.

First, however, we must be clear about what purpose these models serve.

All models are wrong - if they are not, they fail to serve as models.

|

.. raw:: html

   <iframe width="744" height="400" src="https://www.youtube.com/embed/sxxkcd29J2I" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

The King's final model in the video above is *overfitted*.
It fails to serve as a generalization of *countries*, because it *is* a country.
It's important to keep in mind that we seek *generalizations* rather than *perfect alignment* when we work with predictive models.

Deep Neural Networks
--------------------

Deep neural networks, or Artificial Neural Networks (many names, same thing), are networks of interconnected nodes.

.. figure:: _static/ann-structure.png
    :width: 400px
    :align: center
    :alt: Structure of a deep neural network
    :figclass: align-center

    Structure of a Deep Neural Network

Each node contains two elements: a *sum function* that adds all incoming connections together, and an *activation function*.
We will get back to the activation function in a later section.

The diagram above shows a *fully-connected* neural network, but there are many other designs.
For this lecture we will keep things simple, and stick with the design above.

The network above holds two *input nodes* (`x0` and `x1`).
These nodes are special in that they don't have sum and activation functions; they simply holds the input data.

Next follows two *hidden layers*.
The nodes in the hidden layers all contains sum and activation functions, but the activation functions might be different in different layers.
There might be any number of hidden layers.

Finally, there is an *output layer* (`h`).
The output layer represents the output of our hypothesis function.

You will also notice the connections between the layers.
Each connection represents a number.
These *coefficients* usually start up as small random numbers, but we are going to update them during the training process.
We need to train the model because they need to learn from the *data*; they are useless until they are trained.

Training a Deep Neural Network
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The training of a deep neural network is a two-step process, prediction and back-propagation.

First we make predictions.
These predictions are likely very bad, because the model is still not trained.

We record the error by comparing our predictions with the ground truth, and use that error to adjust the coefficients of the model via back-propagation.
The back-propagation process needs an *optimizer*, such as gradient descent from last lecture, but there are other (and better) optimizers available.

We will not cover the training process in detail, as it is requires familiarity with vector and matrix algebra.

Yann LeCun et al. offers a very good introduction to deep learning:

LeCun, Yann, Yoshua Bengio, and Geoffrey Hinton. "Deep learning." nature 521.7553 (2015): 436-444.

.. note:: Read the *Deep Learning* article by LeCun et al.

A more involved article goes into back-propagation on a deeper level (not required reading):

LeCun, Yann A., et al. "Efficient backprop." Neural networks: Tricks of the trade. Springer, Berlin, Heidelberg, 2012. 9-48.

Activation Functions
^^^^^^^^^^^^^^^^^^^^

Activation functions play a crucial role in deep neural networks' ability to approximate non-linear functions.
This is described in the no-so-easy-to-read paper below by Leshno et al.
Don't worry though, you only have to remember the title!

Leshno, Moshe, et al. "Multilayer feedforward networks with a nonpolynomial activation function can approximate any function." Neural networks 6.6 (1993): 861-867.

.. note:: Give the article by Leshno et al. a quick read.
   You don't have to understand every detail, but you must be familiar with the article and its conclusion.

To understand what this means let's observe something very simple like the `XOR` function.

.. csv-table:: XOR Function
   :widths: 30, 30, 30
   :align: center

   "X", "Y", "X ^ Y"
   "0", "0", "0"
   "0", "1", "1"
   "1", "0", "1"
   "1", "1", "0"

The XOR function is takes two arguments (`x` and `y`) and produces and output `x ^ y`.

We can plot the function:

.. figure:: _static/xor-scatter.png
    :align: center
    :alt: xor function
    :figclass: align-center

    XOR Scatter Plot

Observe the XOR scatter plot.
Try to draw a function that separates the blue and the orange dots.

You will realize that you need to draw something that is *not a linear function*.
What you are drawing is a *decision boundary*.

In their essence, models like the linear regressor we saw last week can never learn such a function.
They can, however, combine several linear functions (e.g., to have three areas in the XOR plot).
But this is not nearly as powerful as truly non-linear models.

So back to the activation functions - below you can see three of the most common activation functions for deep neural networks.

Each of the activation functions are presented by their function (denoted sigma) and their derivative function (denoted sigma prime).

.. math::

   \sigma = \frac{1}{1 + e^{-z}}

   \sigma' = z (1 - z)

.. code-block:: python

   def sigmoid(z, prime=False):
      if prime:
         return z * (1 - z)
      return 1 / (1 + np.exp(-z))

.. figure:: _static/sigmoid.png
    :align: center
    :alt: Sigmoid
    :figclass: align-center

    Sigmoid Activation Function

The sigmoid activation function has an output range of 0 to 1.
The sigmoid function is also known as the logistic function, hence classification is known as *logistic regression*.

Sigmoid works well in many cases, especially as a final output function in classification, where you want activations in the 0 to 1 interval.

The sigmoid activation function can suffer from *diminishing gradients* because of the output interval.

.. math::

   \sigma = tanh(z)

   \sigma' = 1 - z^2

.. code-block:: python

   def tanh(z, prime=False):
      if prime:
         return 1 - z ** 2
      return np.tanh(z)

.. figure:: _static/tanh.png
    :align: center
    :alt: tanh
    :figclass: align-center

    tanh (Hyperbolic Tangent) Activation Function

`tanh` can be a better choice in larger networks.
The output range of -1 to 1 is less likely to diminish the gradients.

.. math::

   \sigma = max(0, z)

   \sigma' = \begin{cases}
               0 \text{ for } z \leq 0\\
               z \text{ for } z \gt 0\\
            \end{cases}

.. code-block:: python

   def relu(z, prime=False):
      if prime:
         if z <= 0:
               return 0
         else:
               return 1
      return np.maximum(0, z)

.. figure:: _static/relu.png
    :align: center
    :alt: ReLU
    :figclass: align-center

    ReLU (Rectified Linear Unit) Activation Function

The ReLU activation functions has an output range of 0 to infinity.
It might suffer from both diminishing gradients and *exploding gradients*, which is the opposite: ever increasing gradients.

There is a *leaky ReLU* function that alleviates some of the negative properties of the ReLU activation function:

.. math::

   \sigma = max(\epsilon z, z)

   \sigma' = \begin{cases}
               -\epsilon \text{ for } z \leq 0\\
               z \text{ for } z \gt 0\\
            \end{cases}

Epsilon is some small number.

.. code-block:: python

   def leaky_relu(z, prime=False):
      epsilon = 0.1 # epsilon should be much smaller
      if prime:
         if z <= 0:
               return -epsilon
         else:
               return 1
      return np.maximum(z * epsilon, z)

.. figure:: _static/leaky-relu.png
    :align: center
    :alt: Leaky ReLU
    :figclass: align-center

    Leaky ReLU (Rectified Linear Unit) Activation Function (exaggerated)



Training a Deep Neural Network to Predict XOR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Let's try to build a model that predicts the XOR function.

Now, we only have four samples, so we can't split into a training set and a test set, but we are just interested in getting the model to work in this case.

.. code-block:: python

   import numpy as np
   from sklearn.neural_network import MLPClassifier

   X = np.array([[0,0], [0,1], [1, 0], [1, 1]])
   y = np.array([0, 1, 1, 0])

   mlpc = MLPClassifier(max_iter=5000)
   mlpc.fit(X, y)

   mlpc.predict(X)

In the listing above, `X` is a two-dimensional matrix.
By convention, we name the input data `X`.

`y` is the *ground truth* that we are trying to approximate.

The model itself represents our hypothesis function, so:

.. code-block:: python

   h = mlpc.predict(X)

If we run the script we get two outputs:

.. code-block:: text

   MLPClassifier(activation='relu', alpha=0.0001, batch_size='auto', beta_1=0.9,
               beta_2=0.999, early_stopping=False, epsilon=1e-08,
               hidden_layer_sizes=(100,), learning_rate='constant',
               learning_rate_init=0.001, max_iter=5000, momentum=0.9,
               n_iter_no_change=10, nesterovs_momentum=True, power_t=0.5,
               random_state=None, shuffle=True, solver='adam', tol=0.0001,
               validation_fraction=0.1, verbose=False, warm_start=False)

This describes the configuration of the model - all these parameters are *hyper-parameters*.

The next output is the `h`:

.. code-block:: python

   array([0, 1, 1, 0])

If you match the output with `X` you will see that our model represents the XOR function.

When we instantiated the model we gave a hyper-parameter `max_iter=5000`.
The default value for this hyper-parameter is 200, but it turns out that because our training set is so small, the model can't converge on an approximation with this setting.
Thus, we have to do more iterations to converge.

.. note:: **Assignment 12**

    Explore sklearn for other classifier models.
    Read their documentation and try to train them for the XOR problem.

    *PS: Take care using the correct models: you can't use classification models for regression and vice versa.*

.. note:: **Assignment 13**

    sklearn comes with a few data sets ready for machine learning.

    Take a look at the documentation: `Dataset loading utilities <https://scikit-learn.org/stable/datasets/index.html>`_

    Explore the data sets and try to make predictions using some of the models you found in assignment 12.

    *PS: Take care using the correct models: you can't use classification models for regression and vice versa.*

.. note:: If you have wondered about the term *regression problem* you are not the only one.
   The term is widely used in the literature, but deviates from its original meaning.
   Regression basically means *going back to a less developed state*, but we use the term about optimizing models for continuous (parametric) data.
   It gets even worse: classification is also called *logistic regression*.
   These are unfortunate names, but nevertheless the terms used in data science.

Cost Functions
--------------

We briefly touched the subject of cost functions when we looked at optimization of gradient descent.
As we were dealing with a regression problem, we limited ourselves to a cost function for regression.

We basically deal with two different cost functions: one for regression problems and one for classification problems.

Optimization and Cost Functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Recall the cost function that we observed when we studied gradient descent:

.. figure:: _static/gd-cost-of-approx.png
    :align: center
    :alt: Simple Regression
    :figclass: align-center

As you can see, the cost function is ever decreasing between iterations.
If you observe a fluctuating or increasing cost function between iterations, something is wrong.
You should probably reduce the learning rate, but there might be other problems too.

Regression Cost Function
^^^^^^^^^^^^^^^^^^^^^^^^

We have already seen the cost function for regression problems:

.. math::

   J_\theta(X) = -\frac{1}{m}\sum(h_\theta(X)-Y)^2

Let's try to break it down:

The term :math:`h_\theta(X)-Y` is the difference between our hypothesis function :math:`h_\theta(X)` as a function of :math:`X` and the *ground truth* :math:`Y`.
That is: the difference between our approximated values and the real-world values.

Just like we saw with the *RMSE* we square this number to avoid having positive and negative values even each other out.

That represents the error of each sample, so we sum them all up with :math:`\Sigma` and divide by the number of samples :math:`m` so get an average.
Because we are dealing with an error we make the term negative with the minus sign.

Classification Cost Function
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The classification cost function looks even more scary, but it's really quite simple and identical to the regression cost function.

.. math::

   J_\theta(X)=-\frac{1}{m}\sum(Y log(h_\theta(X)) + (1-Y) log(1 - h\theta(X))

Because we are dealing with classification, we want the error to come out either :math:`0` for correct prediction or :math:`1` for wrong prediction.

Inside the sum function there are two terms :math:`Y log(h_\theta(X))` and :math:`(1-Y) log(1 - h\theta(X)`.
Each of these terms will come out either :math:`0` or :math:`1` because they are multiplied by :math:`Y` - the ground truth, which is exactly :math:`0` or :math:`1`.

If you hold up the terms agains the confusion matrix, you should see that whith a true positive the first term comes out :math:`1` on a second term also comes out :math:`1` - so an error of :math:`0` just as intended.
You can continue inserting the four combinations from the confusion matrix to calculate the errors.

Regularization
^^^^^^^^^^^^^^

Because modern optimization functions are really powerful, we have to be careful not to overfit the model.
That can happen if we have a *variance problem*.

To alleviate that, we can use a *regularization term*:

.. math::

   \frac{\lambda}{2m}\sum\theta^2

:math:`\lambda` is a small positive number that we use to adjust the regularization term.

Because the term correlates positively with the learning parameters :math:`\theta`, and we add this to the cost function, we force the optimizer to keep the learning parameters small.
This ironically makes it harder for the optimizer to optimize the learning parameters, but it helps not overfitting the model.

The cost functions for regression and classification thus looks like this, respectively:

.. math::

   J_\theta(X) = -\frac{1}{m}\sum(h_\theta(X)-Y)^2 + \frac{\lambda}{2m}\sum\theta^2

.. math::

   J_\theta(X) = -\frac{1}{m}\sum(Y log(h_\theta(X)) + (1-Y) log(1 - h\theta(X)) + \frac{\lambda}{2m}\sum\theta^2

Performance Measures for Regression Models
------------------------------------------

As we saw in the *Regression - Full Project* lecture, regression model performance measures are usually:

*Root Mean Square Error*

.. math::

   \sqrt{ \frac{1}{m} \Sigma_{i=1}^{m}( h(x^{(i)}) - y^{(i)})^2 }

or *Mean Absolute Error*:

.. math::

   \frac{1}{m} \Sigma_{i=1}^{m} \vert h(x^{(i)}) - y^{(i)} \vert

Performance Measures for Classification Models
----------------------------------------------

Performance measures for classification problems is a bit more tricky.

Please read the paper:

`Sokolova, Marina, Nathalie Japkowicz, and Stan Szpakowicz. "Beyond accuracy, F-score and ROC: a family of discriminant measures for performance evaluation." Australasian joint conference on artificial intelligence. Springer, Berlin, Heidelberg, 2006. <https://www.aaai.org/Papers/Workshops/2006/WS-06-06/WS06-06-006.pdf>`_

As you can tell from the Sokolova paper, choosing the wrong performance measure can even lead you to the wrong conclusion, so this is a crucial topic.

Below you can see a rough implementation of several classification performance measures:

.. code:: python

   def evaluate(H, Y, beta=1.0):
      tp = sum((Y == H) * (Y == 1) * 1)
      tn = sum((Y == H) * (Y == 0) * 1)
      fp = sum((Y != H) * (Y == 0) * 1)
      fn = sum((Y != H) * (Y == 1) * 1)

      accuracy = (tp + tn) / (tp + fp + fn + tn)
      sensitivity = tp / (tp + fn)
      specificity = tn / (fp + tn)
      precision = tp / (tp + fp)
      recall = sensitivity
      f_score = ( (beta**2 + 1) * precision * recall) / (beta**2 * precision + recall)
      auc = (sensitivity + specificity) / 2
      youden = sensitivity - (1 - specificity)
      p_plus = sensitivity / (1 - specificity)
      p_minus = (1 - sensitivity) / specificity
      dp = (np.sqrt(3) / np.pi) * (np.log(sensitivity/(1 - sensitivity) + np.log(specificity/(1 - specificity))))

      result = {}
      result["tp"] = tp
      result["tn"] = tn
      result["fp"] = fp
      result["fn"] = fn
      result["accuracy"] = accuracy
      result["sensitivity"] = sensitivity
      result["specificity"] = specificity
      result["precision"] = precision
      result["recall"] = recall
      result["f-score"] = f_score
      result["AUC"] = auc
      result["Youden"] = youden
      result["p+"] = p_plus
      result["p-"] = p_minus
      result["DP"] = dp

      return result

The implementation is using `H` (predictions) and `Y` (ground truth) Numpy arrays, and returns a dictionary with several performance measures.

Below you can see two examples of how wrong your results may be if you use the wrong performance metric.

Example 1 - Very few positives
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

I medicine researchers are often dealing with very few instances of one class in a binary classification scenario.
An example could be when screening for a pathology amongst many samples - fortunately only a small number of the samples are positve for a given pathology.

In this case we are screening 10.000.000 individuals; 150 of them are positive, and 120 are false positive:

.. code:: python

   m  = 10_000_000
   p  = 150
   fp = 120

   y = np.zeros(m)
   h = np.zeros(m)

   # inject positives and 20% true positives, 80% false negatives
   for i in range(p):
      idx = random.randint(0, m - 1)
      y[idx] = 1
      if random.random() > 0.8: h[idx] = 1

   # inject false positive
   for i in range(fp):
      idx = random.randint(0, m - 1)
      h[idx] = 1

Using the `evaluate` function defined above we get an *accuracy* of :math:`0.9999764`.

This appears to be a very good performance, but we are only detecting :math:`20%` of the positives, and we detect 120 false positives.
In other words, accuracy is misleading in this case.

If we use Youden's Index as a performance metric we get :math:`0.22665`, clearly showing that our model is not performing very well.

Example 2 - Complete Randomness
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

What happens if we are dealing with something completely random?

Let's take the example of guessing the flip of a coin.

.. code:: python

   m = 10_000_000
   y = np.random.randint(2, size=m)
   h = np.random.randint(2, size=m)

We flip a coin 10.000.000 times :math:`(y)`, and make a guess on the outcome :math:`(h)`.

We get an accuracy very close to :math:`0.5`, but the Youden's Index is close to zero.
Why is that?
Well, with Youden's Index we get an indication of the *predictive power* of the model.
Simply guessing with no better performance than blind luck does not demonstrate any predictive power.

Models
------

As we have discussed many times throughout this course, it is often not possible to tell up front which model or hyper-parameters will perform the best.
Data Science is an empirical science, meaning we have to get our hands dirty and try things out.

Below we will try a number of different models to see how they perform on a classic machine learning problem: MNIST.

.. raw:: html

   <iframe src="_static/mnist-models.html" width="700px" height="500px"></iframe>

As we shall see, the hyper-parameters for some of these models are chosen blindly.

.. note:: Tip: With a Deep Neural Network (such as the MLPClassifier) you want to narrow down in each hidden layer, meaning the first hidden layer should have less nodes than the input layer, the next hidden layer even fewer, etc.

.. note:: We usually have to make trade-offs when developing models.
   As an example, you might get minuscular better results with one model over another, but the training or prediction time is much worse.
   In such case, you might choose the faster model, depending on the application of the model.

.. warning:: Remember, there are many combinations of hyper-parameters.
   The performance of a model depends on the combination of these hyper-parameters, so you can't optimize one at a time.
   Unfortunately, the number of combinations for most models is pretty high, and each combination might take long time to train.
   While you can grid search for combinations, this is often not possible, simply because it will take too long.
   As you work with models over time you will get better at finding good combinations of hyper-parameters, but you still have to try several combinations to verify your assumptions.

Optimizing Models
-----------------

Optimizing models is something that takes a lot of hands-on experience.
However, the process below is a good starting point.

.. figure:: _static/model-optimize.png
    :align: center
    :alt: Model Optimization Process
    :figclass: align-center

    Model Optimization Process

The process requires your data be split into training, validation, and test sets.

.. note:: **Assignment 14**

    Experiment with the code given in the Week 16 lecture for training models on the MNIST data set.

    Try to find some good hyper-parameters for the various models.
    You should use the sklearn documentation to help you understand the hyper-parameters.

    Focus on the Random Forest and MLP models.

.. note:: **Assignment 15**

    Try to apply the various performance metrics given in the *evaluate* function in the Week 16 lecture.

    Having read the Sokolova paper, how do you interpret the results?

.. note:: **Assignment 16**

    The MNIST data set is not structured ideally for machine learning, in that there is only one output value for ten labels.

    Restructure the data set so you have ten output values, one for each label, that is either 0 or 1.

    Train a few models with this refined data set - can you get better results?
