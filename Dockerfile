FROM python:3.8-alpine

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/requirements.txt

RUN apk upgrade --update
RUN apk add entr make
RUN pip install -r requirements.txt
RUN pip install sphinx-autobuild
