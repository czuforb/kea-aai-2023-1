Lecture 1
=========

Who Are We?
-----------

**Henrik Strøm** (`hstr@kea.dk <mailto:hstr@kea.dk>`_)

* Lecturer at KEA
* PhD Fellow at Aalborg University
* M.Sc. in Engineering (cand. polyt.)
* Master in Information and Communication Technologies (mICT)
* Specialize in machine learning, artificial intelligence, and data science
* Worked in Denmark, China, Thailand; also projects in USA, Singapore
* Worked in major enterprise industry, financial sector, start-up
* Been at KEA since May 1st, 2018

**All contact via Teams, please!**

Please present yourself:

* Name
* Expectations for this course


Quick Poll
----------


How many of you ...

* Know Python?
* Have taken a class in artificial intelligence or machine learning?
* Know about dimensionality reduction, cluster analysis, learning algorithms, how to handle bias and variance problems, and statistics?
* Have taken a advanced level math class?


About the Applied Artificial Intelligence Course
------------------------------------------------

Please see :ref:`About the Applied Artificial IntelligenceCourse<about>` for course details.


Getting Help
------------

Use Teams for posting questions between lectures.

You are expected to take part in the discourse on Teams.
Help each other by posting questions and answers on Teams.

Between lectures I will only answer questions already posted on Teams.


GitLab
------

The website that you are looking at is the course materials website.
We use this website instead of textbooks, as you would otherwise have to buy several expensive books.

This website is hosted on GitLab, and you will need to register with the invitation sent to you on your KEA email address in order to use all the functionalities.
We will also use a repository for handing in some of the assignments, as this makes peer-review much easier.


Software Needed for this Course
-------------------------------

You need the following software - make sure you have it installed and that it is working:

* Git - you can use the terminal or a GUI
* Python 3.9 or newer


Python Environments
-------------------

.. note:: If you are running Windows please install `Linux Subsystem for Windows <https://docs.microsoft.com/en-us/windows/wsl/install-win10>`_. My instructions and ability to help will only cover UNIX-like systems.

In this course we will be using Python 3.9 or newer.

.. code-block:: bash

   $ python3 -V
   Python 3.9.1

To separate Python installations and installed packages for different projects we usually create separate environments:

.. code-block:: bash

   $ mkdir ~/py-env
   $ python3 -m venv py-env/aai

Once you have created an environment you can activate it:

.. code-block:: bash

   $ source py-env/aai/bin/activate
   (aai) $

The environment name in parenthesis indicate that the environment is activated.

If you put all your environments in one folder it is easy to configure most editors and IDEs too look for environments in that folder, e.g. `~/py-env/`

You can then install packages into your environment without changing your computer's system level installation of Python, e.g. to install NumPy and Pandas:

.. code-block:: bash

   (aai) $ pip install numpy pandas


About Python
------------

This is a YouTube Python course.
It's quite entertaining, and more than covers our use of Python in this course.

`Python Programming Tutorials <https://www.youtube.com/watch?v=bY6m6_IIN94&list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-&index=1>`_

