Course Materials
================

Everything in the course repository is considered curriculum.

Materials - Required Readings
-----------------------------

`garychl. "How to Compare Machine Learning Algorithms" <https://towardsdatascience.com/how-to-compare-machine-learning-algorithms-ccc266c4777>`_

Géron, Aurélien. "Hands-On Machine Learning with Scikit-Learn and TensorFlow." 2017. Chapters 2 - 3.

`Goldstein, AJ. "Deconstructing Data Science: Breaking The Complex Craft Into It’s Simplest Parts" <https://medium.com/the-mission/deconstructing-data-science-breaking-the-complex-craft-into-its-simplest-parts-15b15420df21>`_

LeCun, Yann, Yoshua Bengio, and Geoffrey Hinton. "Deep learning." nature 521.7553 (2015): 436-444.

Leshno, Moshe, et al. "Multilayer feedforward networks with a nonpolynomial activation function can approximate any function." Neural networks 6.6 (1993): 861-867.

Ng, Andrew. "What artificial intelligence can and can’t do right now." Harvard Business Review 9 (2016).

Nunamaker Jr, Jay F., Minder Chen, and Titus DM Purdin. "Systems development in information systems research." Journal of management information systems 7.3 (1990): 89-106.

Pena, José M., Jose Antonio Lozano, and Pedro Larranaga. "An empirical comparison of four initialization methods for the k-means algorithm." Pattern recognition letters 20.10 (1999): 1027-1040.

Rajpurkar, Pranav, et al. "Chexnet: Radiologist-level pneumonia detection on chest x-rays with deep learning." arXiv preprint arXiv:1711.05225 (2017).

`Seif, George. "5 Quick and Easy Data Visualizations in Python with Code" <https://towardsdatascience.com/5-quick-and-easy-data-visualizations-in-python-with-code-a2284bae952f>`_

`Sokolova, Marina, Nathalie Japkowicz, and Stan Szpakowicz. "Beyond accuracy, F-score and ROC: a family of discriminant measures for performance evaluation." Australasian joint conference on artificial intelligence. Springer, Berlin, Heidelberg, 2006. <https://www.aaai.org/Papers/Workshops/2006/WS-06-06/WS06-06-006.pdf>`_

Strøm, Henrik. "Essay - Experts Perceptions of Artificial Intelligence." 2017.

Strøm, Henrik, Steven Albury, and Lene Tolstrup Sørensen. "Machine Learning Performance Metrics and Diagnostic Context in Radiology." 2018 11th CMI International Conference: Prospects and Challenges Towards Developing a Digital Economy within the EU. IEEE, 2018.

`Taylor, Courtney. "The Difference Between Descriptive and Inferential Statistics" <https://www.thoughtco.com/differences-in-descriptive-and-inferential-statistics-3126224>`_

`Tayo, Benjamin Obi. "Problem Framing: The Most Difficult Stage of a Machine Learning Project Workflow" <https://medium.com/towards-artificial-intelligence/problem-framing-the-most-difficult-stage-of-machine-learning-1a7f208ea414>`_

Zhu, Yangyong, and Yun Xiong. "Defining data science." arXiv preprint arXiv:1501.05039 (2015).

.. note:: You can find papers that are not publicly available in the Course Materials folder in the repository.

Extra Materials - Not Required Readings - Free on the Internet
--------------------------------------------------------------

These sources might help you do projects or you might otherwise have an interest in them.
They are **not** required reading for the course.

`Baker, Lee. "Correlation is not Causation" <https://www.chisquaredinnovations.com/lps/correlation-is-not-causation/>`_

`Caffo, Brian. "Statistical Inference for Data Science" <https://leanpub.com/LittleInferenceBook>`_

`Downey, Allen B. "Think Bayes" <https://greenteapress.com/wp/think-bayes/>`_

`Downey, Allen B. "Think Complexity, 2nd Edition" <https://greenteapress.com/wp/think-complexity-2e/>`_

`Downey, Allen B. "Think DSP - Digital Signal Processing in Python" <https://greenteapress.com/wp/think-dsp/>`_

`Downey, Allen B. "Think Python, 2nd Edition" <https://greenteapress.com/wp/think-python-2e/>`_

`Hastie, Trevor, Robert Tibshirani, Jerome Friedman. "Elements of Statistical Learning: Data Mining, Inference, and Prediction, 2nd Edition" <https://web.stanford.edu/~hastie/ElemStatLearn/index.html>`_

LeCun, Yann A., et al. "Efficient backprop." Neural networks: Tricks of the trade. Springer, Berlin, Heidelberg, 2012. 9-48.

`Legg, Shane, and Marcus Hutter. "A collection of definitions of intelligence." Frontiers in Artificial Intelligence and applications 157 (2007): 17. <https://arxiv.org/pdf/0706.3639.pdf%20a%20collection%20of%20definitions%20of%20intelligence>`_

`Mahajan, Sanjoy. "Street-Fighting Mathematics - The Art of Educated Guessing and Opportunistic Problem Solving" <https://mitpress.mit.edu/books/street-fighting-mathematics>`_

`Turing, Alan M. "Computing machinery and intelligence." Parsing the Turing Test. Springer, Dordrecht, 2009. 23-65. <http://cogprints.org/499/1/turing.html>`_
