Lecture 17
==========


In this lecture we will look at yet another type of models used in *supervised learning*, namely *tree based algorithms*, or more specifically *Decision Trees* and *Random Forests*.

But before we can do that we need to understand a few basic principles.

Decision Trees are build of two kinds of nodes: *decision nodes* and *leaf nodes*.
A decision node contains a *splitting criterion*.
You can think of this criterion as a test we perform on one of the features.
Leaf nodes contains samples that adhere to the criteria in the decision nodes above it.
Consequently, the root node must be a decision node.


.. figure:: _static/decision-tree.png
    :align: center
    :alt: An example decision tree.
    :figclass: align-center

    An example decision tree.


In the following section we will look at two important aspects of decision trees, namely the measures used for the decision nodes to split into subtrees.
Keeping this in mind may help you understand the purpose of that section.

Once we understand the underlying theory of the splitting criterion we will continue dive into the decision trees themselves.


.. note:: In this lecture we will limit ourselves to classification trees.
   Tree algorithms can also be used in a regression setting, but this involves integral calculus that you are probably not familiar with.
   However, we strive to build an intuition for how a tree algorithm works, so dealing only with classification trees will not be a problem.


Entropy and Gini
----------------

Let's start by looking at an example.

Your friend want to play a game of dice.
It's an honest, six sided dice.
If your friend makes a 6 you lose 3 kroner, if not, you make 1 kroner.
Should you take this bet?

Let's put this information into a table to better understand.


.. list-table:: Game of Dice
    :widths: 50 25 25
    :header-rows: 1

    * -
      - 6
      - Not 6
    * - Probability :math:`P(X)`
      - :math:`\frac{1}{6}`
      - :math:`\frac{5}{6}`
    * - Outcome
      - :math:`-3`
      - :math:`1`


We can calculate the expected value of the game by multiplying the probability with the outcome for all possible outcomes:


.. math::

    \sum x P(X=x) = \bigg(-3 \times \frac{1}{6}\bigg) + \bigg(1 \times \frac{5}{6}\bigg) = -\frac{1}{2} + \frac{5}{6} = \frac{1}{3}


So, should you take the bet?
Well, if you play the game one time, the worst case scenario is that you will lose 3 kroner.
If you play the game twice, worst case scenario you lose 6 kroner.

But *over time*, the *expected outcome* will approximate :math:`\frac{1}{3}`.

Let us do a Monte Carlo simulation to see the outcome over a number of games:


.. code-block:: python

    import matplotlib.pyplot as plt
    import random
    from itertools import accumulate

    ROLLS = 100

    roll_dice = lambda : random.randint(1, 6)
    outcome = lambda i: -3 if i == 6 else 1

    x = [x for x in range(ROLLS)]

    y = [outcome(roll_dice()) for _ in range(ROLLS)]
    y = accumulate(y)
    y = [y/i if not i == 0 else y for i, y in enumerate(y)]

    plt.plot(x, y)
    plt.plot(x, [1/3 for _ in range(ROLLS)])


.. figure:: _static/monte-carlo.png
    :align: center
    :alt: Monte Carlo simulation of Game of Dice
    :figclass: align-center

    Monte Carlo simulation of Game of Dice.


As you can see, we slowly approximate :math:`\frac{1}{3}` the more times we play the game.
You can try play the game several times, and you will see very different outcomes.
However, if you increase the variable `ROLLS` to roll the dice more times, you will clearly see the tendency of approximation towards :math:`\frac{1}{3}`.

A Monte Carlo simulation can be a very good tool to *sanity-check* your results.
In our case our results are verified by the Monte Carlo simulation.

Let's update the table with the expected values:


.. list-table:: Game of Dice
    :widths: 50 25 25
    :header-rows: 1

    * -
      - 6
      - Not 6
    * - Probability :math:`P(X)`
      - :math:`\frac{1}{6}`
      - :math:`\frac{5}{6}`
    * - Outcome
      - :math:`-3`
      - :math:`1`
    * - Expected Value
      - :math:`-\frac{1}{2}`
      - :math:`\frac{5}{6}`


If we roll the dice and we get a six, the probability of that happening is :math:`\frac{1}{6}`.
This is low probability, so we could say we get a high degree of *surprise* when that happens.
On the other hand, if we do *not* get a six, the probability of that happening is :math:`\frac{5}{6}`, so a very high probability.
Because of the high probability we get a low degree of *surprise*.

We can see that the concept of *surprise* is somewhat *inversely related* to probability.
We could write this up as :math:`S = \frac{1}{P(X=x)}`.
However, consider the probability of :math:`P(X=x) = 1`, that is a certain outcome, we are not surprised at all, but :math:`S = \frac{1}{P(X=x)} = \frac{1}{1} = 1` means we are very surprised.
That does not work well, so instead we will define surprise as:


.. math::

    S = log_2\bigg(\frac{1}{P(X=x)}\bigg)


With this, the certain case above gives :math:`S = log_2\big(\frac{1}{P(X=x)}\big) = log_2(1) = 0`.
So no surprise there.


.. code-block:: python

    import numpy as np

    surprise = lambda x: np.log2(1/x) if not x == 0 else np.Inf

    x = np.linspace(0, 1, 100)

    y = np.vectorize(surprise)(x)

    plt.plot(x,y)
    plt.xlabel('P(X=x)')
    plt.ylabel('Surprise')


.. figure:: _static/surprise.png
    :align: center
    :alt: Surprise as a function of probability.
    :figclass: align-center

    Surprise as a function of probability.


Now if you are paying attention you might realize that :math:`log_2(0)` is undefined, so how is that useful?
Well, that represents the case of *no probability*, so it makes sense that we can not talk about surprise in that case.

If you are wondering why we use :math:`log_2()` rather than :math:`ln()` or :math:`log_{10}()` it is because we are dealing with *binary entropy*, as there are only *two discrete outcomes*.


Let's update the table with the calculated surprise.


.. list-table:: Game of Dice
    :widths: 50 25 25
    :header-rows: 1

    * -
      - 6
      - Not 6
    * - Probability :math:`P(X)`
      - :math:`\frac{1}{6}`
      - :math:`\frac{5}{6}`
    * - Outcome
      - :math:`-3`
      - :math:`1`
    * - Expected Value
      - :math:`-\frac{1}{2}`
      - :math:`\frac{5}{6}`
    * - Surprise :math:`log_2\big(\frac{1}{P(X=x)}\big)`
      - :math:`2.58`
      - :math:`0.26`


Now that we understand the concept of surprise we can finally get to the point: *what is entropy?*

We can define *entropy* as:


.. math::

    E(S) = \sum x P(X=x) \\= \sum log_2\bigg(\frac{1}{p(x)}\bigg) p(x) \\= \sum -p(x) log_2\big(p(x)\big) \\= - \sum p(x) log_2\bigg(p(x)\bigg)


In other words, *entropy is the average surprise we can expect* - this is easy to see from the first equation.
However, when you see the equation for entropy, it is usually in the last form.


.. note:: **Assignment:** What is the entropy of an honest dice (six sides)?

.. note:: **Assignment:** What is the entropy of an honest coin (heads/tail)?


Now that we have come all this way it is a good time to let you know that most of the time we will actually not use entropy in decision trees.
The reason is that the :math:`log()` operation is quite slow, and we can express an almost identical function with much less computational effort: the *Gini impurity*.

The *Gini impurity* is given by the formula:


.. math::

    G = 1 - \sum p^2(x)


.. note:: In *regression trees* we often use *MSE* or *Chi Square* instead of entropy and Gini.


Let's again look at an example.

Supposed we produce coins of very poor quality, their chance of landing on heads or tail is very different.
We sample ten coins and flip them 100 times each.

.. code-block:: python

    >>> COINS = 10
    >>> heads = (np.random.rand(COINS) * 100).astype(int)
    >>> heads

    array([ 1,  3, 48, 75, 25, 87, 54, 26, 29, 29])

    >>> tails = 100 - heads
    >>> tails

    array([99, 97, 52, 25, 75, 13, 46, 74, 71, 71])

    >>> entropy = lambda x, y: -(x/(x+y) * np.log2(x/(x+y)) + y/(x+y) * np.log2(y/(x+y)))
    >>> entropy(heads, tails)

    array([0.08079314, 0.19439186, 0.99884554, 0.81127812, 0.81127812,
       0.55743819, 0.99537844, 0.82674637, 0.86872125, 0.86872125])

    >>> gini = lambda x, y: 1 - ((x/(x+y))**2 + (y/(x+y))**2)
    >>> gini(heads, tails)

    array([0.0198, 0.0582, 0.4992, 0.375 , 0.375 , 0.2262, 0.4968, 0.3848,
       0.4118, 0.4118])


As you can see, the closer we are to a fair coin, the higher the entropy and Gini.
Let's try to plot the data:

.. code-block:: python

    >>> plt.scatter((heads/heads+tails), entropy(heads, tails), label='entropy')
    >>> plt.scatter((heads/heads+tails), gini(heads, tails), label='gini')
    >>> plt.xlabel('p(x)')
    >>> plt.legend()
    >>> plt.show()


.. figure:: _static/entropy-gini-10.png
    :align: center
    :alt: Entropy and Gini impurity plot
    :figclass: align-center

    Entropy and Gini impurity plot


It's difficult to see the pattern with only ten data points, so let's try with 100:

.. figure:: _static/entropy-gini-100.png
    :align: center
    :alt: Entropy and Gini impurity with 100 samples
    :figclass: align-center

    Entropy and Gini impurity with 100 samples


As you can see, the entropy output range is :math:`[0,1]`, and the Gini output range is :math:`[0, 0.5]`.
If we double the Gini values we can see how closely the resemble each other:


.. figure:: _static/entropy-double-gini.png
    :align: center
    :alt: Double Gini impurity values to show resemblence of entropy
    :figclass: align-center

    Double Gini impurity values to show resemblence of entropy


Because these two functions resemble each other so much, and the computational cost of computing the Gini is far lower than that of computing the entropy we most often use the Gini.
But understand that the Gini is a stand-in for the entropy.


Decision Trees
--------------

Now that we understand entropy and Gini impurity, we can take a look at decision trees.

As mentioned earlier we are dealing with *supervised learning* and more specifically *classification problems*.

We will once again look at the Iris dataset.


.. code-block:: python

    >>> import numpy as np
    >>> from sklearn import datasets

    >>> iris = datasets.load_iris()
    >>> X = iris.data
    >>> X.shape

    (150, 4)

    >>> y = iris.target
    >>> y.shape

    (150,)


As you can see, we have 150 samples, 4 features, and 1 ground truth label.


.. code-block:: python

    >>> labels = iris.target_names
    >>> labels

    array(['setosa', 'versicolor', 'virginica'], dtype='<U10')


Our ground truth label have three distinct values: *sentosa*, *veriscolor*, and *virginica*.

Let's try train the *DecisionTreeClassifier* from *sklearn*:


.. code-block:: python

    >>> from sklearn.tree import DecisionTreeClassifier, plot_tree
    >>> from sklearn.model_selection import train_test_split


As always, we split the dataset into *training*, *validation*, and *test*:


.. code-block:: python

    >>> X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15)
    >>> X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.15)

    >>> X_train.shape, y_train.shape, X_val.shape, y_val.shape, X_test.shape, y_test.shape

    ((107, 4), (107,), (20, 4), (20,), (23, 4), (23,))

    >>> models = [
    >>>    ('Decision Tree', DecisionTreeClassifier()),
    >>> ]

    >>> for name, model in models:
    >>>    print(f'** {name}')
    >>>    model.fit(X_train, y_train)
    >>>    train_pred = model.predict(X_train)
    >>>    val_pred = model.predict(X_val)
    >>>    print(evaluate(train_pred, y_train)['f-score'])
    >>>    print(evaluate(val_pred, y_val)['f-score'])

    ** Decision Tree
    1.0
    1.0


With decision trees, we will often see a perfect results on the training set, and with very small and homogeneous datasets like the Iris dataset, also on the validation and test set.

By the way, we are using the *evaluate* function from earlier lectures.

Now let's try to inspect the trained model:


.. code-block:: python

    >>> plot_tree(models[0][1])


.. figure:: _static/iris-decision-tree.png
    :align: center
    :alt: Decision tree for the Iris dataset.
    :figclass: align-center

    Decision tree for the Iris dataset.


In the root node we start out with the 107 samples from the training set.
On the :math:`X_3` feature we set a criterion of :math:`X_3 \le 0.75` with a Gini value of :math:`0.666`.

This separates 33 samples (left branch) with a Gini of :math:`0`, so this leaf node contains only samples with an :math:`X_3 \le 0.75` and *they all have the same label*.
No further procession is needed on this branch.

The right branch again set a criterion on the :math:`X_3` feature :math:`X_3 \le 1,65`.

The model continues inserting decision nodes to obtain the lowest possible entropy/Gini some criterion is met, it could be:

* leaf nodes contains only one label
* a threshold tree depth has been reached
* the entropy/Gini is below a threshold level


In the two latter cases the leaf node may contain a mix of different labels.
This is to avoid ending up with leaf nodes containing only one or very few samples.


.. note:: **Assignment:** What implications does it have that a leaf node contains more than one label?


.. note:: **Assignment:** Try to configure the decision tree from the example above to use entropy rather than Gini.


Random Forest
-------------

One of the problems with decision trees is that they are very good at *memorizing data*, but very poor at *generalizing from data*.
During this course we have talked a lot about how we try to make *generalized models* rather than *precise representations* from data.

In the example above we also saw that we got perfect performance on both the training set and the validation set; something that should raise a red flag.

Fortunately, *Random Forests* come to the rescue.

Random Forest is a so-called *ensemle* model, which means that it is a model build from a large number of distinct models.
In this case, a random forest is build from an ensemble of decision trees.

There are a few steps in making a random forest:

* Bootstrap a new dataset by sampling from the dataset with replacement
* Chose a random set of features from the dataset
* Build a decision tree based on the bootstrapped dataset with the selected features

Continue this process until you have a large number of decision trees.

This set of decision trees is a Random Forest.

You might wander what *sample with replacement* means.
It simply means that you randomly pick samples from the original dataset for your bootstrapped dataset, but doing so *you don't remove the sample from the original dataset*.
Doing so means that you can end up with duplicates in the bootstrapped dataset.

The act of only selecting a subset of features for the bootstrapped dataset somehow resembles the way we used *Dropout* layers in deep neural networks.

It turns out that random forests perform much better than decision trees, while still training and predicting very fast.

Random forests are often a good choice if you don't have a lot of samples.
It might be difficult to train a deep neural network in such a scenario.


.. note:: **Assignment:** Try to add random forest to the example above.


.. note:: **Assignmnet:** Try to train both a decision tree and a random forest on the MNIST dataset.
   This is a much more complex dataset, and you would expect the random forest to perform much better than the decision tree.
   You can also try other datasets.
   Try to play around with the configuration of the models.
