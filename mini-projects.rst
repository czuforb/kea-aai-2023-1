Mini-Projects
=============


Mini-Project 1
--------------

In this mini-project do will do a statistical analysis of a data set from this page:

https://ourworldindata.org/extreme-poverty

You must prepare a 5 minutes presentation of your analysis for the end of the day.

Your analysis should cover:

* frame the problem
* formulation of a hypothesis and a null-hypothesis
* relevant descriptive statistical analysis
* relevant inferential statistical analysis

.. note::

   You should do some domain research to make sure you have an understanding of the data set.

   You will find implementations of various statistical tests in Python libraries.
