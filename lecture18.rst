Lecture 18
==========


In this lecture we will take a look at *Reinforcement Learning*.

Reinforcement learning is different from supervised learning and unsupervised learning in that we are learning from interaction with an environment.


.. figure:: _static/rl_agent_environment.png
    :align: center
    :alt: The Agent/Environment feedback loop
    :figclass: align-center

    The Agent/Environment feedback loop.


In this formalism the *agent* takes an *action* on the *environment*, and then receives an *observation* or *new state* from the environment.
The agent also receives a *reward* to indicate the *value* of said action.


Markov Chains
-------------

To help us reason about the agent/environment concept we will take a look at *Markov chains*.

Markov chains are often used when dealing with *probabilities*, in that they provide a nice and simple way of describing a system in a very intuitive way.

Let's take the example of flipping a coin.
If we are dealing with an honest coin the probability of landing on heads and tails is identically 50%.

We can illustrate this with a Markov chain.


.. figure:: _static/markov_chain_coin_flip.png
    :align: center
    :alt: Markov chain for flipping an honest coin.
    :figclass: align-center

    Markov chain for flipping an honest coin.


.. note:: **Assignment:** Make a Markov chain for a Danish traffic light.


There's a few things to take note of:

* At all times, the system is in a well-defined state
* The system can only be in one state at any given time
* The transitions describes any possible state change


We are dealing with *discrete* environments in the two scenarios we have looked at so far.
In these environments, the actions also take place in one discrete *step*.

In a discrete environment, there must necessarily be a finite number of possible *states*.
We call the set of all possible states the *state space*.

There must also be a finite number of *actions*, to no surprise the set of all possible actions is called the *action space*.

Even though the state space and action space is finite, it might still be infeasible to deal with the state and action space in a discrete manner.
Many games like *chess* and *go* are examples of this.


Frozen Lake by Hand
-------------------

Let's go through a reinforcement learning example, and see how it works in more details.



.. figure:: _static/frozen-lake.png
    :align: center
    :alt: Frozen Lake.
    :figclass: align-center

    Frozen Lake.


The *Frozen Lake* game contains a discrete environment.
The state space :math:`S = \{ 1 .. 16 \}` contains 16 discrete states, namely the player being located in one of 16 squares.

If the player enters square 6, 8, 12 or 13 he dies.
If he reaches square 16 he wins.

The player always start in square 1, and from the beginning the player knows nothing about the environment.
The action space is :math:`A = \{ 0 .. 3 \}` representing the movements left, down, right, and up.

So how can we learn from the environment?
Well, we just try to take some action.


Epsilon Greedy
~~~~~~~~~~~~~~


To learn over time, we introduce a variable :math:`\epsilon` (epsilon).
The :math:`\epsilon` value start out high, say :math:`\epsilon = 1.0`, with a *decay* of say :math:`\epsilon_{decay} = 0.995`, and a minimum value :math:`\epsilon_{min} = 0.01`.

The :math:`\epsilon` value determines the *exploratory* vs *exploitative* behavior of the agent.
To begin with, the agent know nothing about the environment, and must be exploratory in nature to learn from the environment.
As the agent learns more, it can benefit from prior learning by becoming more exploitative.
This happens by adjusting continuously adjusting :math:`\epsilon` by the decay.

When :math:`\epsilon = 0.5` it will be exploratory in 50% of the actions.


Determining Value
~~~~~~~~~~~~~~~~~


The agent receives a *reward* from each action.
We want the agent to reach the goal as fast as possible, so the agent must not waste time going back and forth.

We could have a negative reward on each step, making detours less rewarding for the agent.
However, we usually apply the concept of a *reward discount factor*, making immediate reward have higher value than rewards further into the future.


Bellman Equation
~~~~~~~~~~~~~~~~


The Bellman equation is used to calculate the value of an action, assuming all future actions will be optimal.

The equation reads:


.. math:: Q_{i+1}(s,a) = R + \gamma \max_{a'}Q_i(s',a')


The :math:`Q` function for step :math:`i + 1` takes a state :math:`s` and an action :math:`a` and computes the future reward, which consists of the immediate reward :math:`R` plus the discount factor :math:`\gamma` multiplied by the optimal future reward.

With this, our goal is to determine the *optimal Q-function* :math:`Q^*`.


Q-Learning
----------


One approach we can take in reinforcement learning is *Q-learning*.

Q-learning works well in small, discrete state/action spaces.
We shall now apply it to the frozen lake example from above.

We will be using the *Gym Library*, which is a collection of interactive environments for reinforcement learning.

You can find the general website here: https://www.gymlibrary.dev

And the frozen lake example here: https://www.gymlibrary.dev/environments/toy_text/frozen_lake/


.. note:: **Lab Excercise:** See the notebook in `course-materials/reinforcement-learning/frozen-movie.ipynb`


Deep Q-Learning
---------------


In some cases the state or action space becomes so big that making a Q-table as we did with frozen lake is not a practical solution.

In such case we can use deep neural networks.

It is a bit more complicated than it may seem at first.
You might think you can just feed the state to the deep neural network and get the optimal action as an output.
This turns out not to work very well.

Instead we will use two deep neural networks, a *Q-Net* and a *Target Q-Net*.
The problem is a bit like something you might have tried in programming: if you iterate over a list of elements and you simultaneously make updates to the same list, things can become very unstable.
We face the same kind of problem here, thus the need for two deep neural networks.


.. note:: **Lab Excercise:** See the notebook in `course-materials/reinforcement-learning/deep-q-lunar.ipynb`
