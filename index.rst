.. Applied Artificial Intelligence - Spring 2023 documentation master file, created by
   sphinx-quickstart on Thu Jan 30 07:10:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Applied Artificial Intelligence - Spring 2023!
=========================================================

.. image:: _static/kea-logo.png
    :width: 744px
    :align: center
    :height: 457px
    :alt: KEA logo

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   course-materials
   lecture-plan
   mandatory-assignments
   mini-projects
   lecture01
   lecture02
   lecture03
   lecture04
   lecture05
   lecture06
   lecture07
   lecture08
   lecture09
   lecture10
   lecture11
   lecture12
   lecture13
   lecture14
   lecture15
   lecture16
   lecture17
   lecture18


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
