Lecture 14
==========

In this lecture we will look at *anomaly detection*.
Anomaly detection is part of *unsupervised learning*.

Anomaly detection is about finding samples that are different from the *normal* samples.
*Normal* in this case just means *similar to the majority of samples*.

You might wonder why not do a classification: *normal* vs. *anomalous* samples.
While this might work in some cases, often we don't know exactly *what* an anomaly would look like, so it would be difficult to find samples for training.
It might be that there are far too few anomalous samples to be able to train a classification model.
It might also be that we simply don't have a labeled dataset, and therefor are limited to what we can achieve with unsupervised learning.


ECG Anomaly Detection
---------------------

An *electrocardiogram* (ECG/EKG) is a recording of the heart's electrical activity.
Without going into details of the ECG itself it is sufficient to know that a physician can tell a lot about a patient's health condition from reading an ECG.

While we can easily establish what a healthy ECG should look like, there are hundreds if not thousands of ways an ECG can be *anomalous*.

Anomaly detection can quickly identify *anomalous* samples for further examination, but not provide information about *why* the ECG is not normal.
While that may sound very limiting, it is in fact still a great help.


.. figure:: _static/ecg-autoencoder.png
    :align: center
    :alt: An autoencoder trained on human ECG.
    :figclass: align-center

    An autoencoder trained on human ECG.


In the figure above you see an representation of an *autoencoder*.
Autoencoders are very powerful anomaly detectors.

The *encoder* and *decoder* are both deep neural networks.
The decoder is a mirrored representation of the encoder.

The encoder compresses the input samples into a *code* representation.
The decoder is then trained to *reconstruct* samples from the code.
As the decoder is a mirror of the encoder, the reconstruction expands the sample back to its original dimensions.

The encoding process is lossy, so the decoder is not able to perfectly reconstruct the sample.
However, the decoder will produce a *generalization* of the samples.
Now, if we only train the autoencoder with *normal* samples, it will be much better at reconstruction normal samples than anomalous samples.

This is exactly the idea: because the autoencoder is bad at reconstructing anomalous samples, we will be able to find these samples.

So let's try to implement an autoencoder for this purpose.


Implementing an Autoencoder for Anomaly Detection of ECG
--------------------------------------------------------

As usual we must do the necessary imports.


.. code-block:: python

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    import tensorflow as tf

    from sklearn.metrics import accuracy_score, precision_score, recall_score
    from sklearn.model_selection import train_test_split
    from tensorflow.keras import layers, losses
    from tensorflow.keras.models import Model


We will use Pandas to download the dataset and make the CSV file available in a more accessible format:


.. code-block:: python


    dataframe = pd.read_csv('http://storage.googleapis.com/download.tensorflow.org/data/ecg.csv', header=None)
    raw_data = dataframe.values
    dataframe.head()


If you run the cell above you will see that the samples consists of 141 columns, where the last column is the label.
The first 140 columns is a time-series representation of the heart's electric state at some point in time.

We will separate the labels and make a train/test split:


.. code-block:: python

    # The last element contains the labels
    labels = raw_data[:, -1]

    # The other data points are the electrocadriogram data
    data = raw_data[:, 0:-1]

    train_data, test_data, train_labels, test_labels = train_test_split(
        data, labels, test_size=0.2, random_state=21
    )


We will also do a normalization of the data, and convert to `float32` for faster performance:


.. code-block:: python

    min_val = tf.reduce_min(train_data)
    max_val = tf.reduce_max(train_data)

    train_data = (train_data - min_val) / (max_val - min_val)
    test_data = (test_data - min_val) / (max_val - min_val)

    train_data = tf.cast(train_data, tf.float32)
    test_data = tf.cast(test_data, tf.float32)


Next we separate normal from anomalous samples:


.. code-block:: python

    train_labels = train_labels.astype(bool)
    test_labels = test_labels.astype(bool)

    normal_train_data = train_data[train_labels]
    normal_test_data = test_data[test_labels]

    anomalous_train_data = train_data[~train_labels]
    anomalous_test_data = test_data[~test_labels]


Let's try to plot a normal sample:


.. code-block:: python

    plt.grid()
    plt.plot(np.arange(140), normal_train_data[0])
    plt.title("A Normal ECG")
    plt.show()


.. figure:: _static/ecg-normal.png
    :align: center
    :alt: A normal ECG.
    :figclass: align-center

    A normal ECG.


And a anomalous ECG:


.. code-block:: python

    plt.grid()
    plt.plot(np.arange(140), anomalous_train_data[0])
    plt.title("An Anomalous ECG")
    plt.show()


.. figure:: _static/ecg-not-normal.png
    :align: center
    :alt: A anomalous ECG.
    :figclass: align-center

    A anomalous ECG.


Now we can define our model:


.. code-block:: python

    class AnomalyDetector(Model):
        def __init__(self):
            super(AnomalyDetector, self).__init__()

            self.encoder = tf.keras.Sequential([
                layers.Dense(32, activation="relu"),
                layers.Dense(16, activation="relu"),
                layers.Dense(8, activation="relu")])

            self.decoder = tf.keras.Sequential([
                layers.Dense(16, activation="relu"),
                layers.Dense(32, activation="relu"),
                layers.Dense(140, activation="sigmoid")])

        def call(self, x):
            encoded = self.encoder(x)
            decoded = self.decoder(encoded)
            return decoded

    autoencoder = AnomalyDetector()


As you can see, the decoder is a mirror of the encoder.
The 8-node layer is not missing from the decoder.
This layer is the code representation.


.. code-block:: python

    autoencoder.compile(optimizer='adam', loss='mae')


    history = autoencoder.fit(normal_train_data, normal_train_data,
            epochs=20,
            batch_size=512,
            validation_data=(test_data, test_data),
            shuffle=True)


Note that we are only using normal samples for training.
We are only training our autoencoder to reconstruct normal samples; consequently it will perform very poor on anomalous samples, which is exactly what we want.

We are using a *Mean Absolute Error* here (`mae`).

Let's plot the loss:


.. code-block:: python

    plt.plot(history.history["loss"], label="Training Loss")
    plt.plot(history.history["val_loss"], label="Validation Loss")
    plt.legend()


.. figure:: _static/ecg-training-loss.png
    :align: center
    :alt: ECG training and validation loss.
    :figclass: align-center

    ECG training and validation loss.


The loss plot looks fine in that we see a flattening curve at the end of training.

Now that our autoencoder knows how to reconstruct normal samples let's try to have it do just that:


.. code-block:: python

    encoded_data = autoencoder.encoder(normal_test_data).numpy()
    decoded_data = autoencoder.decoder(encoded_data).numpy()

    plt.plot(normal_test_data[0], 'b')
    plt.plot(decoded_data[0], 'r')
    plt.fill_between(np.arange(140), decoded_data[0], normal_test_data[0], color='lightcoral')
    plt.legend(labels=["Input", "Reconstruction", "Error"])
    plt.show()


.. figure:: _static/ecg-reconstruct-normal.png
    :align: center
    :alt: Reconstruction of a normal ECG.
    :figclass: align-center

    Reconstruction of a normal ECG.


Also, let's try to reconstruct a anomalous sample:

.. code-block:: python

    encoded_data = autoencoder.encoder(anomalous_test_data).numpy()
    decoded_data = autoencoder.decoder(encoded_data).numpy()

    plt.plot(anomalous_test_data[0], 'b')
    plt.plot(decoded_data[0], 'r')
    plt.fill_between(np.arange(140), decoded_data[0], anomalous_test_data[0], color='lightcoral')
    plt.legend(labels=["Input", "Reconstruction", "Error"])
    plt.show()


.. figure:: _static/ecg-reconstruct-not-normal.png
    :align: center
    :alt: Reconstruction of a anomalous ECG.
    :figclass: align-center

    Reconstruction of a anomalous ECG.


I can't really tell a normal from a anomalous ECG, but that's OK, our model can help us with that.

Let's try to plot a histogram on the distribution of error in the training data:

.. code-block:: python

    reconstructions = autoencoder.predict(normal_train_data)
    train_loss = tf.keras.losses.mae(reconstructions, normal_train_data)

    plt.hist(train_loss[None,:], bins=50)
    plt.xlabel("Train loss")
    plt.ylabel("No of examples")
    plt.show()


.. figure:: _static/ecg-train-hist.png
    :align: center
    :alt: Histogram of training loss for normal ECG.
    :figclass: align-center

    Histogram of training loss for normal ECG.


It can be hard to tell exactly, but around :math:`0.02 .. 0.03` seems to split the distribution, but we can compute the *threshold* as :math:`\mu + \sqrt{\frac{\sum(x - \mu)^2}{n}} = 0.033`.

.. code-block:: python

    threshold = np.mean(train_loss) + np.std(train_loss)
    print("Threshold: ", threshold)

    Threshold:  0.03277553


Now let's try to do the same for anomalous samples:


.. code-block:: python

    reconstructions = autoencoder.predict(anomalous_test_data)
    test_loss = tf.keras.losses.mae(reconstructions, anomalous_test_data)

    plt.hist(test_loss[None, :], bins=50)
    plt.xlabel("Test loss")
    plt.ylabel("No of examples")
    plt.show()


.. figure:: _static/ecg-test-hist.png
    :align: center
    :alt: Histogram of training loss for anomalous ECG.
    :figclass: align-center

    Histogram of training loss for anomalous ECG.


We can see the 50/50 split is much higher here, maybe around :math:`0.05`.
Also, at or below the level of normal samples, at :math:`0.033`, we see only a handful of samples.
This error can then serve as anomaly detector.

Finally, let's get some general performance measures on the model:


.. code-block:: python

    def predict(model, data, threshold):
        reconstructions = model(data)
        loss = tf.keras.losses.mae(reconstructions, data)
        return tf.math.less(loss, threshold)

    def print_stats(predictions, labels):
        print("Accuracy = {}".format(accuracy_score(labels, predictions)))
        print("Precision = {}".format(precision_score(labels, predictions)))
        print("Recall = {}".format(recall_score(labels, predictions)))


.. code-block:: python

    preds = predict(autoencoder, test_data, threshold)
    print_stats(preds, test_labels)

    Accuracy = 0.945
    Precision = 0.9922027290448343
    Recall = 0.9089285714285714


Our model turns out to perform pretty well.

The *precision* of :math:`0.99` tells us that when our model identifies a normal sample, we can be quite certain it is actually a normal sample.

The *recall* of :math:`0.91` tells us that we are detecting quite a few normal samples as anomalous, but given how small a dataset we are working with this seems pretty good.


.. note:: **Assignment:** Can you make the anomaly detector perform better?
